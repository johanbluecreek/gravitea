
use bevy::{
    prelude::*,
    time::Stopwatch,
    window::WindowResized,
    render::camera::RenderTarget,
};
use directories::BaseDirs;
use std::path::PathBuf;

/*
    Constants
*/

pub const TIME_STEP: f32 = 1.0/60.0;
pub const WINDOW_WIDTH: f32 = 1920.0;
pub const WINDOW_HEIGHT: f32 = 1080.0;
pub const INTERACTION_DELAY: f32 = 0.2;

/*
    Components
*/

#[derive(Component)]
pub struct GameCamera;

#[derive(Component)]
pub struct CurrentLevel;

/*
    Resources
*/

pub struct ClickPosition {
    pub pos: Vec2
}

pub struct MousePosition {
    pub world_pos: Vec2,
    pub screen_pos: Vec2,
}

pub struct GameStateTimer(pub Stopwatch);

pub struct PathHandle(pub PathBuf);

/*
    States
*/

#[derive(Clone, Eq, PartialEq, Debug, Hash)]
pub enum GameState {
    Menu,
    Game,
    Switch,
}

#[derive(Clone, Eq, PartialEq, Debug, Hash)]
pub enum NextGameState {
    Game,
    Dummy
}

/*
    Systems
*/

pub fn switch_state(
    mut game_state: ResMut<State<GameState>>,
    mut next_game_state: ResMut<State<NextGameState>>
) {
    match next_game_state.current() {
        NextGameState::Game => {
            next_game_state.overwrite_set(NextGameState::Dummy).unwrap();
            game_state.overwrite_set(GameState::Game).unwrap();
        }
        _ => {}
    }
}

fn setup_camera(mut commands: Commands) {
    commands.spawn()
        .insert_bundle(Camera2dBundle::new_with_far(1000.0))
        .insert(GameCamera);
}

fn update_timers(mut game_state_timer: ResMut<GameStateTimer>, time: Res<Time>) {
    game_state_timer.0.tick(time.delta());
}

fn resize_window(
    mut commands: Commands,
    projection: Query<Entity, With<GameCamera>>,
    mut resize_event: EventReader<WindowResized>
) {
    match resize_event.iter().next() {
        Some(w) => {
            let projection = projection.single();
            commands.entity(projection).despawn();
            let mut camera = Camera2dBundle::new_with_far(1000.0);
            camera.projection.scale = (WINDOW_HEIGHT / w.height).max(WINDOW_WIDTH / w.width);
            commands.spawn_bundle(camera)
                .insert(GameCamera);
        },
        None => ()
    };
}

fn setup_game_folder(
    path_handle: Res<PathHandle>,
) {
    if !path_handle.0.exists() {
        match std::fs::create_dir(&path_handle.0) {_ => {}};
    }
}

fn update_mouse_position(
    wnds: Res<Windows>,
    camera_query: Query<(&Camera, &GlobalTransform), With<GameCamera>>,
    mut mouse_position: ResMut<MousePosition>,
) {
    let (camera, global_transform) = camera_query.single();
    let wnd = if let RenderTarget::Window(id) = camera.target {
        wnds.get(id).unwrap()
    } else {
        wnds.get_primary().unwrap()
    };

    let mut world_pos: Vec2 = Vec2::ZERO;
    let mut screen_position = Vec2::ZERO;
    if let Some(screen_pos) = wnd.cursor_position() {
        let window_size = Vec2::new(wnd.width() as f32, wnd.height() as f32);
        let ndc = (screen_pos / window_size) * 2.0 - Vec2::ONE;
        let ndc_to_world = global_transform.compute_matrix() * camera.projection_matrix().inverse();
        let world_pos3 = ndc_to_world.project_point3(ndc.extend(-1.0));
        world_pos = world_pos3.truncate();
        screen_position = screen_pos;
    }
    mouse_position.screen_pos = screen_position;
    mouse_position.world_pos = world_pos;
}

fn update_click_position(
    mouse_position: Res<MousePosition>,
    mut click_position: ResMut<ClickPosition>,
    mouse: Res<Input<MouseButton>>,
) {
    if mouse.just_pressed(MouseButton::Left) {
        click_position.pos = mouse_position.world_pos.clone();
    }
}

/*
    Plugins
*/

// Labels
#[derive(Debug, Clone, PartialEq, Eq, Hash, SystemLabel)]
pub struct MouseUpdateLabel;

pub struct GeneralMechanicsPlugin;

impl Plugin for GeneralMechanicsPlugin {
    fn build(&self, app: &mut App) {
        // Resources
        app.insert_resource(ClickPosition {pos: Vec2::ZERO})
            .insert_resource(MousePosition {screen_pos: Vec2::ZERO, world_pos: Vec2::ZERO})
            .insert_resource(GameStateTimer(Stopwatch::new()))
            .insert_resource(
                PathHandle(
                    BaseDirs::new().unwrap()
                        .data_local_dir()
                        .join("gravitea")
                )
            );
        // States
        app.add_state(GameState::Menu)
            .add_state(NextGameState::Dummy);
        // Systems
        app.add_startup_system(setup_camera)
            .add_startup_system(setup_game_folder)
            .add_system(update_timers)
            .add_system(resize_window);
        app.add_system_set(
            SystemSet::new()
                .label(MouseUpdateLabel)
                .with_system(update_mouse_position)
                .with_system(
                    update_click_position
                        .after(update_mouse_position)
                )
        );
    }
}
