use bevy::prelude::*;
use crate::mechanics::general::PathHandle;
use serde::{Serialize,Deserialize};
use std::collections::HashMap;

/*
    Resources
*/

pub struct Score(pub f32);
#[derive(Serialize, Deserialize, Debug)]
pub struct HighScore(pub HashMap<i32, f32>);
pub struct CurrentBestScore(pub f32);

/*
    Systems
*/

fn setup_highscore(
    path_handle: Res<PathHandle>,
    mut highscore: ResMut<HighScore>,
) {
    match std::fs::read_to_string(path_handle.0.join("high_score.json")) {
        Ok(s) => {
            let d: HighScore = serde_json::from_str(&s).unwrap();
            highscore.0 = d.0;
        },
        _ => {}
    }
}

/*
    Plugins
*/

pub struct ScoreResourcesPlugin;

impl Plugin for ScoreResourcesPlugin {
    fn build(&self, app: &mut App) {
        // Resources
        app.insert_resource(Score(0.0))
            .insert_resource(HighScore(HashMap::new()))
            .insert_resource(CurrentBestScore(0.0));
        // Systems
        app.add_startup_system(setup_highscore);
    }
}
