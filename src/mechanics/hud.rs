use bevy::{
    prelude::*,
    diagnostic::{Diagnostics, FrameTimeDiagnosticsPlugin}
};

use crate::{
    components::probe::Probe,
    mechanics::{
        general::{CurrentLevel, GameState},
        score::{Score, CurrentBestScore, HighScore},
    },
    physics::general::Velocity,
    assets::assets::FontHandle,
    levels::levels::{LevelData, LevelSelect},
};

/*
    Constants
*/

const FONT_SIZE: f32 = 24.0;

/*
    Components
*/

#[derive(Component)]
pub struct ScoreText;

#[derive(Component)]
pub struct FPSText;

#[derive(Component)]
pub struct SpeedText;

/*
    Systems
*/

fn setup_hud(
    mut commands: Commands,
    fonts: Res<FontHandle>,
) {
    commands.spawn()
        .insert_bundle(TextBundle {
            style: Style {
                position_type: PositionType::Absolute,
                position: UiRect {
                    top: Val::Px(8.0),
                    left: Val::Px(10.0),
                    ..default()
                },
                ..default()
            },
            text: Text {
                sections: vec![
                    TextSection {
                        value: "Score: ".to_string(),
                        style: TextStyle {
                            font: fonts.0.clone(),
                            font_size: FONT_SIZE,
                            color: Color::WHITE,
                        },
                    },
                    TextSection {
                        value: "".to_string(),
                        style: TextStyle {
                            font: fonts.0.clone(),
                            font_size: FONT_SIZE,
                            color: Color::WHITE,
                        },
                    },
                ],
                ..default()
            },
            ..default()
        })
        .insert(ScoreText)
        .insert(CurrentLevel);
}

fn update_scoretext(
    mut query: Query<&mut Text, With<ScoreText>>,
    score: Res<Score>,
    current_best: Res<CurrentBestScore>,
    highscore: Res<HighScore>,
    level_select: Res<State<LevelSelect>>,
) {
    if query.is_empty() {
        return ()
    }
    let mut text = query.single_mut();
    match level_select.current() {
        LevelSelect::Level(current_level) => {
            match highscore.0.get(current_level) {
                Some(high_score) => {
                    if current_best.0 == 0.0 {
                        text.sections[1].value = format!(
                            "{:10.3e} (high score: {:10.3e})"
                        , score.0, high_score);
                    } else {
                        text.sections[1].value = format!(
                            "{:10.3e} (current best: {:10.3e}; high score: {:10.3e})"
                        , score.0, current_best.0, high_score);
                    }
                },
                None => {
                    if current_best.0 == 0.0 {
                        text.sections[1].value = format!(
                            "{:10.3e}"
                        , score.0);
                    } else {
                        text.sections[1].value = format!(
                            "{:10.3e} (current best: {:10.3e})"
                        , score.0, current_best.0);
                    }
                }
            }
        }
    }
}

fn setup_fps(
    mut commands: Commands,
    fonts: Res<FontHandle>,
) {
    commands.spawn()
        .insert_bundle(TextBundle {
            style: Style {
                position_type: PositionType::Absolute,
                position: UiRect {
                    bottom: Val::Px(8.0),
                    left: Val::Px(10.0),
                    ..default()
                },
                ..default()
            },
            text: Text {
                sections: vec![
                    TextSection {
                        value: "FPS: ".to_string(),
                        style: TextStyle {
                            font: fonts.0.clone(),
                            font_size: 12.0,
                            color: Color::WHITE,
                        },
                    },
                    TextSection {
                        value: "".to_string(),
                        style: TextStyle {
                            font: fonts.0.clone(),
                            font_size: 12.0,
                            color: Color::WHITE,
                        },
                    },
                ],
                ..default()
            },
            ..default()
        })
        .insert(FPSText)
        .insert(CurrentLevel);
}

fn update_fps(diagnostics: Res<Diagnostics>, mut query: Query<&mut Text, With<FPSText>>) {
    let mut text = query.single_mut();
    if let Some(fps) = diagnostics.get(FrameTimeDiagnosticsPlugin::FPS) {
        if let Some(average) = fps.average() {
            text.sections[1].value = format!("{:.2}", average);
        }
    }
}

fn setup_speedometer(
    mut commands: Commands,
    fonts: Res<FontHandle>,
) {
    commands.spawn()
    .insert_bundle(TextBundle {
        style: Style {
            position_type: PositionType::Absolute,
            position: UiRect {
                top: Val::Px(36.0),
                left: Val::Px(10.0),
                ..default()
            },
            ..default()
        },
        text: Text {
            sections: vec![
                TextSection {
                    value: "Velocity: ".to_string(),
                    style: TextStyle {
                        font: fonts.0.clone(),
                        font_size: FONT_SIZE/2.0,
                        color: Color::WHITE,
                    },
                },
                TextSection {
                    value: "".to_string(),
                    style: TextStyle {
                        font: fonts.0.clone(),
                        font_size: FONT_SIZE/2.0,
                        color: Color::WHITE,
                    },
                },
                TextSection {
                    value: "%".to_string(),
                    style: TextStyle {
                        font: fonts.0.clone(),
                        font_size: FONT_SIZE/2.0,
                        color: Color::WHITE,
                    },
                },
            ],
            ..default()
        },
        ..default()
    })
    .insert(SpeedText)
    .insert(CurrentLevel);
}

fn update_speedometer(
    mut query: Query<&mut Text, With<SpeedText>>,
    probe: Query<&Velocity, With<Probe>>,
    level_data: Res<LevelData>,
) {
    if query.is_empty() || probe.is_empty() {
        return ()
    }
    let mut text = query.single_mut();
    let vel = probe.single().vec.length()/level_data.physics_constants.c;
    text.sections[1].value = format!("{:.2}", vel*100.0);
}

/*
    Plugins
*/

pub struct HUDPlugin;

impl Plugin for HUDPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set(
                SystemSet::on_enter(GameState::Game)
                    .with_system(setup_hud)
                    .with_system(setup_speedometer)
            )
            .add_system_set(
                SystemSet::on_update(GameState::Game)
                    .with_system(update_scoretext)
                    .with_system(update_speedometer)
            );
    }
}

pub struct FPSPlugin;

impl Plugin for FPSPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set(
                SystemSet::on_enter(GameState::Game)
                    .with_system(setup_fps)
            )
            // Update - always
            .add_system_set(
                SystemSet::on_update(GameState::Game)
                    .with_system(update_fps)
            );
    }
}
