

use bevy::{
    prelude::*,
    app::PluginGroupBuilder,
    diagnostic::FrameTimeDiagnosticsPlugin,
};

use crate::{
    components::{
        probe::ProbeManagementPlugin,
        background::BackgroundHandlingPlugin,
        sources::SourceManagementPlugin,
        path_trace::TracedotManagementPlugin,
        goal::GoalPlugin,
    },
    mechanics::{
        hud::{HUDPlugin, FPSPlugin},
        input::InputHandlingPlugin,
        gameover::GameoverHandlingPlugin,
        general::{
            GeneralMechanicsPlugin, switch_state, GameState,
        },
        border::BorderDrawingPlugin,
        menu::MainMenuPlugin,
        score::ScoreResourcesPlugin,
    },
    physics::physics::PhysicsInteractionPlugin,
    levels::levels::{level_setup, LevelManagementPlugin},
    assets::assets::AssetGenPlugin,
};

struct SwitchPlugin;

impl Plugin for SwitchPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set(
            SystemSet::on_enter(GameState::Switch)
                .with_system(switch_state)
                .with_system(level_setup)
        );
    }
}

/*
    Plugin Groups
*/

pub struct DiagnosticsGroup;

impl PluginGroup for DiagnosticsGroup {
    fn build(&mut self, group: &mut PluginGroupBuilder) {
        group
            .add(FrameTimeDiagnosticsPlugin::default())
            .add(FPSPlugin);
    }
}

pub struct GraviteaPluginsGroup;

impl PluginGroup for GraviteaPluginsGroup {
    fn build(&mut self, group: &mut PluginGroupBuilder) {
        group
            .add(AssetGenPlugin)
            .add(MainMenuPlugin)
            .add(BackgroundHandlingPlugin)
            .add(LevelManagementPlugin)
            .add(BorderDrawingPlugin)
            .add(HUDPlugin)
            .add(SwitchPlugin)
            .add(GeneralMechanicsPlugin)
            .add(InputHandlingPlugin);
        // Physics and interactions
        group
            .add(ProbeManagementPlugin)
            .add(SourceManagementPlugin)
            .add(TracedotManagementPlugin)
            .add(PhysicsInteractionPlugin);
        // Score and winning
        group
            .add(GoalPlugin)
            .add(GameoverHandlingPlugin)
            .add(ScoreResourcesPlugin);
    }
}
