
use bevy::{
    prelude::*,
    sprite::MaterialMesh2dBundle,
};

use crate::{
    levels::levels::LevelData,
    physics::physics::Topology,
    mechanics::general::{
        CurrentLevel, WINDOW_HEIGHT, WINDOW_WIDTH, GameState,
    },
    assets::assets::ColorHandle,
};

/*
    Utilities
*/

struct BorderPart {
    size: Vec2,
    translation: Vec2,
    rotation: Quat,
}

impl BorderPart {
    fn new(size: Vec2, translation: Vec2, rotation: Quat) -> Self {
        BorderPart {
            size: size,
            translation: translation,
            rotation: rotation,
        }
    }
}

trait BorderItems {
    fn get_border_items(&self) -> Vec<BorderPart>;
}

impl BorderItems for Topology {
    fn get_border_items(&self) -> Vec<BorderPart> {
        let mut to_draw: Vec<BorderPart> = vec![];
        match self {
            Topology::Open => {},
            Topology::Boxed(_) => {
                to_draw.push(
                    BorderPart::new(
                        Vec2::new(WINDOW_WIDTH, 2.0),
                        Vec2::new(0.0, WINDOW_HEIGHT/2.0),
                        Quat::from_rotation_x(0.0),
                    )
                );
                to_draw.push(
                    BorderPart::new(
                        Vec2::new(WINDOW_WIDTH, 2.0),
                        Vec2::new(0.0, -WINDOW_HEIGHT/2.0),
                        Quat::from_rotation_x(0.0),
                    )
                );
                to_draw.push(
                    BorderPart::new(
                        Vec2::new(2.0, WINDOW_HEIGHT),
                        Vec2::new(-WINDOW_WIDTH/2.0, 0.0),
                        Quat::from_rotation_y(0.0),
                    )
                );
                to_draw.push(
                    BorderPart::new(
                        Vec2::new(2.0, WINDOW_HEIGHT),
                        Vec2::new(WINDOW_WIDTH/2.0, 0.0),
                        Quat::from_rotation_y(0.0),
                    )
                );
            },
            Topology::Torus => {
                to_draw.push(
                    BorderPart::new(
                        Vec2::new(WINDOW_WIDTH/20.0, 2.0),
                        Vec2::new(0.0, WINDOW_HEIGHT/2.0),
                        Quat::from_rotation_x(0.0),
                    )
                );
                to_draw.push(
                    BorderPart::new(
                        Vec2::new(WINDOW_WIDTH/20.0, 2.0),
                        Vec2::new(0.0, WINDOW_HEIGHT/2.0),
                        Quat::from_rotation_x(0.0),
                    )
                );
                to_draw.push(
                    BorderPart::new(
                        Vec2::new(WINDOW_WIDTH/40.0, 2.0),
                        Vec2::new(0.0+WINDOW_WIDTH/40.0, WINDOW_HEIGHT/2.0),
                        Quat::from_axis_angle(Vec3::Z, std::f32::consts::PI/4.0),
                    )
                );
                to_draw.push(
                    BorderPart::new(
                        Vec2::new(WINDOW_WIDTH/20.0, 2.0),
                        Vec2::new(0.0, -WINDOW_HEIGHT/2.0),
                        Quat::from_rotation_x(0.0)
                    )
                );
                to_draw.push(
                    BorderPart::new(
                        Vec2::new(WINDOW_WIDTH/40.0, 2.0),
                        Vec2::new(0.0+WINDOW_WIDTH/40.0, -WINDOW_HEIGHT/2.0),
                        Quat::from_axis_angle(-Vec3::Z, std::f32::consts::PI/4.0)
                    )
                );
                to_draw.push(
                    BorderPart::new(
                        Vec2::new(2.0, WINDOW_HEIGHT/20.0),
                        Vec2::new(-WINDOW_WIDTH/2.0, 0.0),
                        Quat::from_rotation_y(0.0)
                    )
                );
                to_draw.push(
                    BorderPart::new(
                        Vec2::new(2.0, WINDOW_HEIGHT/40.0),
                        Vec2::new(-WINDOW_WIDTH/2.0, WINDOW_HEIGHT/40.0),
                        Quat::from_axis_angle(Vec3::Z, std::f32::consts::PI/4.0)
                    )
                );
                to_draw.push(
                    BorderPart::new(
                        Vec2::new(2.0, WINDOW_HEIGHT/20.0),
                        Vec2::new(WINDOW_WIDTH/2.0, 0.0),
                        Quat::from_rotation_y(0.0)
                    )
                );
                to_draw.push(
                    BorderPart::new(
                        Vec2::new(2.0, WINDOW_HEIGHT/40.0),
                        Vec2::new(WINDOW_WIDTH/2.0, WINDOW_HEIGHT/40.0),
                        Quat::from_axis_angle(-Vec3::Z, std::f32::consts::PI/4.0)
                    )
                );
            },
            Topology::Klein => {
                to_draw.push(
                    BorderPart::new(
                        Vec2::new(WINDOW_WIDTH/20.0, 2.0),
                        Vec2::new(0.0, WINDOW_HEIGHT/2.0),
                        Quat::from_rotation_x(0.0)
                    )
                );
                to_draw.push(
                    BorderPart::new(
                        Vec2::new(WINDOW_WIDTH/40.0, 2.0),
                        Vec2::new(0.0-WINDOW_WIDTH/40.0, WINDOW_HEIGHT/2.0),
                        Quat::from_axis_angle(-Vec3::Z, std::f32::consts::PI/4.0)
                    )
                );
                to_draw.push(
                    BorderPart::new(
                        Vec2::new(WINDOW_WIDTH/20.0, 2.0),
                        Vec2::new(0.0, -WINDOW_HEIGHT/2.0),
                        Quat::from_rotation_x(0.0)
                    )
                );
                to_draw.push(
                    BorderPart::new(
                        Vec2::new(WINDOW_WIDTH/40.0, 2.0),
                        Vec2::new(0.0+WINDOW_WIDTH/40.0, -WINDOW_HEIGHT/2.0),
                        Quat::from_axis_angle(-Vec3::Z, std::f32::consts::PI/4.0)
                    )
                );
                to_draw.push(
                    BorderPart::new(
                        Vec2::new(2.0, WINDOW_HEIGHT/20.0),
                        Vec2::new(-WINDOW_WIDTH/2.0, 0.0),
                        Quat::from_rotation_y(0.0)
                    )
                );
                to_draw.push(
                    BorderPart::new(
                        Vec2::new(2.0, WINDOW_HEIGHT/40.0),
                        Vec2::new(-WINDOW_WIDTH/2.0, WINDOW_HEIGHT/40.0),
                        Quat::from_axis_angle(Vec3::Z, std::f32::consts::PI/4.0)
                    )
                );
                to_draw.push(
                    BorderPart::new(
                        Vec2::new(2.0, WINDOW_HEIGHT/20.0),
                        Vec2::new(WINDOW_WIDTH/2.0, 0.0),
                        Quat::from_rotation_y(0.0)
                    )
                );
                to_draw.push(
                    BorderPart::new(
                        Vec2::new(2.0, WINDOW_HEIGHT/40.0),
                        Vec2::new(WINDOW_WIDTH/2.0, WINDOW_HEIGHT/40.0),
                        Quat::from_axis_angle(-Vec3::Z, std::f32::consts::PI/4.0)
                    )
                );
            },
        }
        return to_draw
    }
}

/*
    Systems
*/

fn setup_border(
    mut commands: Commands,
    level_data: Res<LevelData>,
    mut meshes: ResMut<Assets<Mesh>>,
    colors: Res<ColorHandle>,
) {
    for border_part in level_data.topology.get_border_items().iter() {
        commands.spawn()
            .insert_bundle(MaterialMesh2dBundle {
                mesh: meshes.add(Mesh::from(shape::Quad{size: border_part.size, ..default()})).into(),
                transform: Transform {
                    translation: border_part.translation.extend(0.0),
                    scale: Vec3::splat(1.0),
                    rotation: border_part.rotation
                },
                material: colors.border.clone(),
                ..default()
            })
            .insert(CurrentLevel);
    }
}

/*
    Plugins
*/

pub struct BorderDrawingPlugin;

impl Plugin for BorderDrawingPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set(
            SystemSet::on_enter(GameState::Game)
                .with_system(setup_border)
        );
    }
}
