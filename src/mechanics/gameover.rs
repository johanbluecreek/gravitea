
use bevy::prelude::*;

use crate::{
    mechanics::{
        general::{
            GameState, GameStateTimer, NextGameState, CurrentLevel,
        },
        menu::MenuState,
        score::{Score, CurrentBestScore, HighScore},
    },
    levels::levels::{LevelSelect, LevelsEnded},
    assets::assets::FontHandle,
    components::probe::OldDragbar,
    physics::physics::Collided,
};

/*
    Components
*/

#[derive(Component)]
pub struct Gameoverbox;

#[derive(Component)]
pub enum BoxButtonAction {
    Replay,
    Menu,
    Next,
}

/*
    Resources
*/

pub struct Won(pub bool);
pub struct Lost(pub bool);

/*
    Systems
*/

fn reset_gameover(
        mut won: ResMut<Won>,
        mut lost: ResMut<Lost>,
        mut score: ResMut<Score>,
        mut collided: ResMut<Collided>
    ) {
    won.0 = false;
    lost.0 = false;
    score.0 = 0.0;
    collided.0 = false;
}

fn draw_gameover_box(
    mut commands: Commands,
    score: Res<Score>,
    won: Res<Won>,
    lost: Res<Lost>,
    gameover_query: Query<&Gameoverbox, With<Gameoverbox>>,
    level_end: Res<LevelsEnded>,
    fonts: Res<FontHandle>,
) {
    if (won.0 || lost.0) && gameover_query.is_empty() {
        let background_node = NodeBundle {
            style: Style {
                margin: UiRect::all(Val::Auto),
                flex_direction: FlexDirection::ColumnReverse,
                align_items: AlignItems::Center,
                ..default()
            },
            color: Color::rgba(0.2, 0.2, 0.2, 0.9).into(),
            ..default()
        };

        let title_text = TextBundle {
            text: Text::from_section(
                if won.0 {"You Won!"} else {"You Crashed."},
                TextStyle {
                    font: fonts.0.clone(),
                    font_size: 36.0,
                    color: Color::rgb(0.9, 0.9, 0.9),
                }
            ),
            style: Style {
                // center button
                margin: UiRect::all(Val::Px(20.0)),
                // horizontally center child text
                justify_content: JustifyContent::Center,
                // vertically center child text
                align_items: AlignItems::Center,
                ..default()
            },
            ..default()
        };

        let subtitle_text = TextBundle {
            text: Text::from_section(
                format!("Score: {:10.3e}", score.0),
                TextStyle {
                    font: fonts.0.clone(),
                    font_size: 24.0,
                    color: Color::rgb(0.9, 0.9, 0.9),
                }
            ),
            style: Style {
                // center button
                margin: UiRect::all(Val::Px(20.0)),
                // horizontally center child text
                justify_content: JustifyContent::Center,
                // vertically center child text
                align_items: AlignItems::Center,
                ..default()
            },
            ..default()
        };

        let button_node = NodeBundle {
            style: Style {
                margin: UiRect::all(Val::Auto),
                flex_direction: FlexDirection::RowReverse,
                align_items: AlignItems::Center,
                ..default()
            },
            color: Color::rgba(0.0, 0.0, 0.0, 0.0).into(),
            ..default()
        };

        let button_bundle = ButtonBundle {
            style: Style {
                size: Size::new(Val::Px(150.0), Val::Px(65.0)),
                // center button
                margin: UiRect::all(Val::Px(20.0)),
                // horizontally center child text
                justify_content: JustifyContent::Center,
                // vertically center child text
                align_items: AlignItems::Center,
                ..default()
            },
            color: Color::rgba(0.1, 0.1, 0.1, 0.8).into(),
            ..default()
        };

        let retry_text = TextBundle {
            text: Text::from_section(
                "Retry",
                TextStyle {
                    font: fonts.0.clone(),
                    font_size: 40.0,
                    color: Color::rgb(0.9, 0.9, 0.9),
                }
            ),
            ..default()
        };

        let menu_text = TextBundle {
            text: Text::from_section(
                "Menu",
                TextStyle {
                    font: fonts.0.clone(),
                    font_size: 40.0,
                    color: Color::rgb(0.9, 0.9, 0.9),
                }
            ),
            ..default()
        };

        commands.spawn()
            .insert_bundle(background_node)
            .with_children(|parent| {
                parent.spawn_bundle(title_text);
                parent.spawn_bundle(subtitle_text);
                parent.spawn_bundle(button_node)
                .with_children(|parent| {
                    // retry button
                    parent.spawn_bundle(button_bundle.clone())
                    .with_children(|parent| {
                        parent.spawn_bundle(retry_text);
                    })
                    .insert(BoxButtonAction::Replay);
                    // menu button
                    parent.spawn_bundle(button_bundle.clone())
                    .with_children(|parent| {
                        parent.spawn_bundle(menu_text);
                    })
                    .insert(BoxButtonAction::Menu);
                    // potential next button
                    if !(level_end.0 || lost.0) {
                        let next_text = TextBundle {
                            text: Text::from_section(
                                "Next",
                                TextStyle {
                                    font: fonts.0.clone(),
                                    font_size: 40.0,
                                    color: Color::rgb(0.9, 0.9, 0.9),
                                }
                            ),
                            ..default()
                        };

                        parent.spawn_bundle(button_bundle.clone())
                        .with_children(|parent| {
                            parent.spawn_bundle(next_text);
                        })
                        .insert(BoxButtonAction::Next);
                    }
                });
            })
        .insert(Gameoverbox)
        .insert(CurrentLevel);
    }
}

fn interact_gameoverbox(
    interaction_query: Query<(&Interaction, &BoxButtonAction),(Changed<Interaction>, With<Button>)>,
    mut game_state: ResMut<State<GameState>>,
    mut menu_state: ResMut<State<MenuState>>,
    mut game_state_timer: ResMut<GameStateTimer>,
    mut next_game_state: ResMut<State<NextGameState>>,
    mut level_select: ResMut<State<LevelSelect>>,
    score: Res<Score>,
    mut current_best: ResMut<CurrentBestScore>,
    mut highscore: ResMut<HighScore>,
    won: Res<Won>,
    old_dragbar_query: Query<Entity, With<OldDragbar>>,
    mut commands: Commands,
) {
    for (interaction, menu_button_action) in interaction_query.iter() {
        if *interaction == Interaction::Clicked {
            let &LevelSelect::Level(current_level) = level_select.current();
            let mut leaving = false;
            match menu_button_action {
                BoxButtonAction::Menu => {
                    game_state.set(GameState::Menu).unwrap();
                    menu_state.set(MenuState::MainMenu).unwrap();
                    game_state_timer.0.reset();
                    leaving = true;
                },
                BoxButtonAction::Replay => {
                    game_state.set(GameState::Switch).unwrap();
                    next_game_state.set(NextGameState::Game).unwrap();
                    game_state_timer.0.reset();
                }
                BoxButtonAction::Next => {
                    current_best.0 = 0.0;
                    game_state.set(GameState::Switch).unwrap();
                    next_game_state.set(NextGameState::Game).unwrap();
                    level_select.set(LevelSelect::Level(current_level+1)).unwrap();
                    game_state_timer.0.reset();
                    leaving = true;
                    for entity in old_dragbar_query.iter() {
                        commands.entity(entity).despawn();
                    }
                }
            }
            if current_best.0 < score.0 && won.0 {
                current_best.0 = score.0;
                highscore.0.entry(current_level)
                    .and_modify(|e|
                        if e < &mut current_best.0 {*e = current_best.0}
                    )
                    .or_insert(current_best.0);
            }
            if leaving {
                current_best.0 = 0.0;
            }
        }
    }
}

/*
    Plugins
*/

pub struct GameoverHandlingPlugin;

impl Plugin for GameoverHandlingPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(Won(false))
            .insert_resource(Lost(false))
            .add_system_set(
                SystemSet::on_enter(GameState::Game)
                    .with_system(reset_gameover)
            )
            // Update - always
            .add_system_set(
                SystemSet::on_update(GameState::Game)
                    .with_system(draw_gameover_box)
                    .with_system(interact_gameoverbox)
            );
    }
}
