
use bevy::{
    prelude::*,
    app::AppExit,
};

use crate::{
    mechanics::{
        general::{
            GameStateTimer, CurrentLevel, GameState, NextGameState,
            PathHandle, INTERACTION_DELAY,
        },
        score::HighScore,
    },
    levels::levels::LevelSelect,
    assets::assets::FontHandle,
};

/*
    Components
*/

#[derive(Component)]
pub struct CurrentMenu;

#[derive(Component)]
enum MenuButtonAction {
    Play,
    LevelSelect,
    Level(i32),
    Controls,
    Return,
    Quit,
}

/*
    Resources
*/

#[derive(Clone, Eq, PartialEq, Debug, Hash)]
pub enum MenuState {
    MainMenu,
    LevelSelect,
    Controls,
    None,
}

/*
    Systems
*/

fn setup_mainmenu(
    mut commands: Commands,
    fonts: Res<FontHandle>,
) {

    let menu_background_node = NodeBundle {
        style: Style {
            margin: UiRect::all(Val::Auto),
            flex_direction: FlexDirection::ColumnReverse,
            align_items: AlignItems::Center,
            ..default()
        },
        color: Color::rgba(0.0, 0.0, 0.0, 0.0).into(),
        ..default()
    };

    let title_text = TextBundle {
        text: Text::from_section(
            "Gravitea",
            TextStyle {
                font: fonts.0.clone(),
                font_size: 56.0,
                color: Color::rgb(0.9, 0.9, 0.9),
            }
        ),
        style: Style {
            // center button
            margin: UiRect::all(Val::Px(100.0)),
            // horizontally center child text
            justify_content: JustifyContent::Center,
            // vertically center child text
            align_items: AlignItems::Center,
            ..default()
        },
        ..default()
    };

    let button_text_style = TextStyle {
        font: fonts.0.clone(),
        font_size: 40.0,
        color: Color::rgb(0.9, 0.9, 0.9),
    };

    let menu_button = ButtonBundle {
        style: Style {
            size: Size::new(Val::Px(250.0), Val::Px(65.0)),
            // center button
            margin: UiRect::all(Val::Px(20.0)),
            // horizontally center child text
            justify_content: JustifyContent::Center,
            // vertically center child text
            align_items: AlignItems::Center,
            ..default()
        },
        color: Color::rgba(0.1, 0.1, 0.1, 0.8).into(),
        ..default()
    };

    let start_button_text = TextBundle {
        text: Text::from_section(
            "Start",
            button_text_style.clone()
        ),
        ..default()
    };

    let level_button_text = TextBundle {
        text: Text::from_section(
            "Select level",
            button_text_style.clone()
        ),
        ..default()
    };

    let controls_button_text = TextBundle {
        text: Text::from_section(
            "Controls",
            button_text_style.clone()
        ),
        ..default()
    };

    let quit_button_text = TextBundle {
        text: Text::from_section(
            "Quit",
            button_text_style.clone()
        ),
        ..default()
    };

    commands.spawn_bundle(menu_background_node)
        .with_children(|parent| {
            // title
            parent.spawn_bundle(title_text);
            // start
            parent.spawn_bundle(menu_button.clone())
                .with_children(|parent| {
                    parent.spawn_bundle(start_button_text);
                })
                .insert(MenuButtonAction::Play);
            // level select
            parent.spawn_bundle(menu_button.clone())
                .with_children(|parent| {
                    parent.spawn_bundle(level_button_text);
                })
                .insert(MenuButtonAction::LevelSelect);
            // controls
            parent.spawn_bundle(menu_button.clone())
                .with_children(|parent| {
                    parent.spawn_bundle(controls_button_text);
                })
                .insert(MenuButtonAction::Controls);
            // quit
            parent.spawn_bundle(menu_button.clone())
                .with_children(|parent| {
                    parent.spawn_bundle(quit_button_text);
                })
                .insert(MenuButtonAction::Quit);
        })
        .insert(CurrentLevel)
        .insert(CurrentMenu);
}

fn setup_levelselection(
    mut commands: Commands,
    fonts: Res<FontHandle>,
    highscore: Res<HighScore>,
) {
    let menu_background_node = NodeBundle {
        style: Style {
            margin: UiRect::all(Val::Auto),
            flex_direction: FlexDirection::ColumnReverse,
            align_items: AlignItems::Center,
            ..default()
        },
        color: Color::rgba(0.0, 0.0, 0.0, 0.0).into(),
        ..default()
    };

    let title_text = TextBundle {
        text: Text::from_section(
            "Select Level",
            TextStyle {
                font: fonts.0.clone(),
                font_size: 56.0,
                color: Color::rgb(0.9, 0.9, 0.9),
            }
        ),
        style: Style {
            // center button
            margin: UiRect::all(Val::Px(100.0)),
            // horizontally center child text
            justify_content: JustifyContent::Center,
            // vertically center child text
            align_items: AlignItems::Center,
            ..default()
        },
        ..default()
    };

    let button_text_style = TextStyle {
        font: fonts.0.clone(),
        font_size: 40.0,
        color: Color::rgb(0.9, 0.9, 0.9),
    };

    let highscore_text_style = TextStyle {
        font: fonts.0.clone(),
        font_size: 18.0,
        color: Color::rgb(0.9, 0.9, 0.9),
    };

    let menu_button = ButtonBundle {
        style: Style {
            size: Size::new(Val::Px(250.0), Val::Px(65.0)),
            // center button
            margin: UiRect::all(Val::Px(20.0)),
            // horizontally center child text
            justify_content: JustifyContent::Center,
            // vertically center child text
            align_items: AlignItems::Center,
            ..default()
        },
        color: Color::rgba(0.1, 0.1, 0.1, 0.8).into(),
        ..default()
    };

    let level_selection_node = NodeBundle {
        style: Style {
            margin: UiRect::all(Val::Auto),
            flex_direction: FlexDirection::Row,
            align_items: AlignItems::Center,
            ..default()
        },
        color: Color::rgba(0.0, 0.0, 0.0, 0.0).into(),
        ..default()
    };

    let level_button = ButtonBundle {
        style: Style {
            size: Size::new(Val::Px(150.0), Val::Px(65.0)),
            // center button
            margin: UiRect::all(Val::Px(20.0)),
            // horizontally center child text
            justify_content: JustifyContent::Center,
            // vertically center child text
            align_items: AlignItems::Center,
            flex_direction: FlexDirection::Column,
            ..default()
        },
        color: Color::rgba(0.1, 0.1, 0.1, 0.8).into(),
        ..default()
    };

    let level_button_text_style = TextStyle {
        font: fonts.0.clone(),
        font_size: 24.0,
        color: Color::rgb(0.9, 0.9, 0.9),
    };

    let return_button_text = TextBundle {
        text: Text::from_section(
            "Return",
            button_text_style.clone()
        ),
        ..default()
    };

    commands.spawn_bundle(menu_background_node)
        .with_children(|parent| {
            // title
            parent.spawn_bundle(title_text);
            parent.spawn_bundle(level_selection_node)
                .with_children(|parent| {
                    for level in 0..LevelSelect::num_levels() {
                        parent.spawn_bundle(level_button.clone())
                            .with_children(|parent| {
                                parent.spawn_bundle(
                                    TextBundle {
                                        text: Text::from_section(
                                            match highscore.0.get(&level) {
                                                Some(highscore) => format!("score: {:10.3e}", highscore),
                                                None => "score: 0.0".to_string()
                                            },
                                            highscore_text_style.clone()
                                        ),
                                        style: Style {
                                            justify_content: JustifyContent::Center,
                                            align_items: AlignItems::Center,
                                            ..default()
                                        },
                                        ..default()
                                    }
                                );
                                parent.spawn_bundle(
                                    TextBundle {
                                        text: Text::from_section(
                                            format!("{}", LevelSelect::Level(level).get_name()),
                                            level_button_text_style.clone()
                                        ),
                                        ..default()
                                    }
                                );
                            })
                            .insert(MenuButtonAction::Level(level));
                    }
                });
            // return
            parent.spawn_bundle(menu_button.clone())
                .with_children(|parent| {
                    parent.spawn_bundle(return_button_text);
                })
                .insert(MenuButtonAction::Return);
        })
        .insert(CurrentLevel)
        .insert(CurrentMenu);
}

fn setup_controls(
    mut commands: Commands,
    fonts: Res<FontHandle>,
) {
    let menu_background_node = NodeBundle {
        style: Style {
            margin: UiRect::all(Val::Auto),
            flex_direction: FlexDirection::ColumnReverse,
            align_items: AlignItems::Center,
            ..default()
        },
        color: Color::rgba(0.0, 0.0, 0.0, 0.0).into(),
        ..default()
    };

    let title_text = TextBundle {
        text: Text::from_section(
            "Controls",
            TextStyle {
                font: fonts.0.clone(),
                font_size: 56.0,
                color: Color::rgb(0.9, 0.9, 0.9),
            }
        ),
        style: Style {
            // center button
            margin: UiRect::all(Val::Px(60.0)),
            // horizontally center child text
            justify_content: JustifyContent::Center,
            // vertically center child text
            align_items: AlignItems::Center,
            ..default()
        },
        ..default()
    };

    let button_text_style = TextStyle {
        font: fonts.0.clone(),
        font_size: 40.0,
        color: Color::rgb(0.9, 0.9, 0.9),
    };

    let menu_button = ButtonBundle {
        style: Style {
            size: Size::new(Val::Px(250.0), Val::Px(65.0)),
            // center button
            margin: UiRect::all(Val::Px(20.0)),
            // horizontally center child text
            justify_content: JustifyContent::Center,
            // vertically center child text
            align_items: AlignItems::Center,
            ..default()
        },
        color: Color::rgba(0.1, 0.1, 0.1, 0.8).into(),
        ..default()
    };

    let controls_display_node = NodeBundle {
        style: Style {
            margin: UiRect::all(Val::Auto),
            flex_direction: FlexDirection::Row,
            align_items: AlignItems::Center,
            ..default()
        },
        color: Color::rgba(0.0, 0.0, 0.0, 0.0).into(),
        ..default()
    };

    let controls_text_style = TextStyle {
        font: fonts.0.clone(),
        font_size: 28.0,
        color: Color::rgb(0.9, 0.9, 0.9),
    };

    let return_button_text = TextBundle {
        text: Text::from_section(
            "Return",
            button_text_style.clone()
        ),
        ..default()
    };

    let left_tab_node = NodeBundle {
        style: Style {
            margin: UiRect::all(Val::Auto),
            align_items: AlignItems::FlexEnd,
            flex_direction: FlexDirection::Column,
            ..default()
        },
        color: Color::rgba(0.0, 0.0, 0.0, 0.0).into(),
        ..default()
    };

    let right_tab_node = NodeBundle {
        style: Style {
            margin: UiRect::all(Val::Auto),
            align_items: AlignItems::FlexStart,
            flex_direction: FlexDirection::Column,
            ..default()
        },
        color: Color::rgba(0.0, 0.0, 0.0, 0.0).into(),
        ..default()
    };

    let text_style = Style {
        margin: UiRect::all(Val::Px(20.0)),
        align_items: AlignItems::Center,
        ..default()
    };

    commands.spawn_bundle(menu_background_node)
        .with_children(|parent| {
            // title
            parent.spawn_bundle(title_text);
            parent.spawn_bundle(controls_display_node)
                .with_children(|parent| {
                    parent.spawn_bundle(left_tab_node)
                        .with_children(|parent| {
                            parent.spawn_bundle(TextBundle {
                                text: Text::from_section(
                                    "x",
                                    controls_text_style.clone()
                                ),
                                style: text_style.clone(),
                                ..default()
                            });
                            parent.spawn_bundle(TextBundle {
                                text: Text::from_section(
                                    "r",
                                    controls_text_style.clone()
                                ),
                                style: text_style.clone(),
                                ..default()
                            });
                            parent.spawn_bundle(TextBundle {
                                text: Text::from_section(
                                    "Click and drag",
                                    controls_text_style.clone()
                                ),
                                style: text_style.clone(),
                                ..default()
                            });
                        });
                    parent.spawn_bundle(right_tab_node)
                        .with_children(|parent| {
                            parent.spawn_bundle(TextBundle {
                                text: Text::from_section(
                                    "Exit current level/exit game",
                                    controls_text_style.clone()
                                ),
                                style: text_style.clone(),
                                ..default()
                            });
                            parent.spawn_bundle(TextBundle {
                                text: Text::from_section(
                                    "Restart current level",
                                    controls_text_style.clone()
                                ),
                                style: text_style.clone(),
                                ..default()
                            });
                            parent.spawn_bundle(TextBundle {
                                text: Text::from_section(
                                    "Fling the probe",
                                    controls_text_style.clone()
                                ),
                                style: text_style.clone(),
                                ..default()
                            });
                        });
                });
            // return
            parent.spawn_bundle(menu_button.clone())
                .with_children(|parent| {
                    parent.spawn_bundle(return_button_text);
                })
                .insert(MenuButtonAction::Return);
        })
        .insert(CurrentLevel)
        .insert(CurrentMenu);
}

fn interact_menu(
    interaction_query: Query<(&Interaction, &MenuButtonAction),(Changed<Interaction>, With<Button>)>,
    mut game_state: ResMut<State<GameState>>,
    mut next_game_state: ResMut<State<NextGameState>>,
    mut menu_state: ResMut<State<MenuState>>,
    mut level_select: ResMut<State<LevelSelect>>,
    mut game_state_timer: ResMut<GameStateTimer>,
    mut app_exit_events: EventWriter<AppExit>,
    path_handle: Res<PathHandle>,
    highscore: Res<HighScore>,
) {
    for (interaction, menu_button_action) in interaction_query.iter() {
        if *interaction == Interaction::Clicked {
            match menu_button_action {
                MenuButtonAction::Play => {
                    if game_state_timer.0.elapsed_secs() > INTERACTION_DELAY {
                        game_state.set(GameState::Game).unwrap();
                        menu_state.set(MenuState::None).unwrap();
                        game_state_timer.0.reset();
                    }
                }
                MenuButtonAction::LevelSelect => {
                    if game_state_timer.0.elapsed_secs() > INTERACTION_DELAY {
                        menu_state.set(MenuState::LevelSelect).unwrap();
                        game_state_timer.0.reset();
                    }
                },
                MenuButtonAction::Level(selected_level) => {
                    if game_state_timer.0.elapsed_secs() > INTERACTION_DELAY {
                        let current_level = match level_select.current() { LevelSelect::Level(j) => {*j} };
                        if current_level != *selected_level {
                            level_select.set(LevelSelect::Level(*selected_level)).unwrap();
                        }
                        menu_state.set(MenuState::None).unwrap();
                        game_state.set(GameState::Switch).unwrap();
                        next_game_state.set(NextGameState::Game).unwrap();
                        game_state_timer.0.reset();
                    }
                },
                MenuButtonAction::Controls => {
                    if game_state_timer.0.elapsed_secs() > INTERACTION_DELAY {
                        menu_state.set(MenuState::Controls).unwrap();
                        game_state_timer.0.reset();
                    }
                },
                MenuButtonAction::Return => {
                    if game_state_timer.0.elapsed_secs() > INTERACTION_DELAY {
                        menu_state.set(MenuState::MainMenu).unwrap();
                        game_state_timer.0.reset();
                    }
                },
                MenuButtonAction::Quit => {
                    match std::fs::write(
                        path_handle.0.join("high_score.json"),
                        serde_json::to_string(highscore.as_ref()).unwrap()
                    ) { _ => {} };
                    app_exit_events.send(AppExit)
                },
            }
        }
    }
}

fn despawn_current_menu(mut commands: Commands, query: Query<Entity, With<CurrentMenu>>) {
    for entity in query.iter() {
        commands.entity(entity).despawn_recursive();
    }
}

/*
    Plugins
*/

pub struct MainMenuPlugin;

impl Plugin for MainMenuPlugin {
    fn build(&self, app: &mut App) {
        // Menu State
        app.add_state(MenuState::MainMenu);
        // Menu structure
        app
            // Start-up
            .add_system_set(
                SystemSet::on_enter(MenuState::MainMenu)
                    .with_system(setup_mainmenu)
            )
            .add_system_set(
                SystemSet::on_enter(MenuState::LevelSelect)
                    .with_system(setup_levelselection)
            )
            .add_system_set(
                SystemSet::on_enter(MenuState::Controls)
                    .with_system(setup_controls)
            )
            // Update - always
            .add_system_set(
                SystemSet::on_update(GameState::Menu)
                    .with_system(interact_menu)
            )
            // Exit
            .add_system_set(
                SystemSet::on_exit(MenuState::MainMenu)
                    .with_system(despawn_current_menu)
            )
            .add_system_set(
                SystemSet::on_exit(MenuState::LevelSelect)
                    .with_system(despawn_current_menu)
            )
            .add_system_set(
                SystemSet::on_exit(MenuState::Controls)
                    .with_system(despawn_current_menu)
            );
    }
}
