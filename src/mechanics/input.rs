use bevy::{
    prelude::*,
    input::{keyboard::KeyCode, Input},
    app::AppExit,
    sprite::MaterialMesh2dBundle,
};

use crate::{
    components::probe::ShootRes,
    mechanics::{
        general::{
            GameState, NextGameState, GameStateTimer, PathHandle,
            INTERACTION_DELAY, WINDOW_WIDTH, WINDOW_HEIGHT,
        },
        score::{Score, CurrentBestScore, HighScore},
        menu::MenuState,
        gameover::Won,
    },
    levels::levels::LevelSelect,
    assets::assets::ColorHandle,
};

/*
    Components
*/

#[derive(Component)]
pub struct DevModeIndicator;

/*
    Systems
*/

fn exit_current_state_input_system(
    mut game_state: ResMut<State<GameState>>,
    mut menu_state: ResMut<State<MenuState>>,
    keyboard_input: Res<Input<KeyCode>>,
    mut app_exit_events: EventWriter<AppExit>,
    mut game_state_timer: ResMut<GameStateTimer>,
    won: Res<Won>,
    score: Res<Score>,
    mut current_best: ResMut<CurrentBestScore>,
    mut highscore: ResMut<HighScore>,
    path_handle: Res<PathHandle>,
    level_select: Res<State<LevelSelect>>,
) {
    if game_state_timer.0.elapsed_secs() < INTERACTION_DELAY {
        return ()
    }
    if keyboard_input.pressed(KeyCode::X) {
        match game_state.current() {
            GameState::Game => {
                game_state.set(GameState::Menu).unwrap();
                menu_state.set(MenuState::MainMenu).unwrap();
                game_state_timer.0.reset();
                if current_best.0 < score.0 && won.0 {
                    // if we are on the win-screen, make sure to update scores
                    current_best.0 = score.0;
                    let &LevelSelect::Level(current_level) = level_select.current();
                    highscore.0.entry(current_level)
                        .and_modify(|e|
                            if e < &mut current_best.0 {*e = current_best.0}
                        )
                        .or_insert(current_best.0);
                }
                // and reset current score before exit
                current_best.0 = 0.0;
            }
            GameState::Menu => {
                match std::fs::write(
                    path_handle.0.join("high_score.json"),
                    serde_json::to_string(highscore.as_ref()).unwrap()
                ) { _ => {} };
                app_exit_events.send(AppExit)
            }
            _ => {}
        }
    }
}

fn replay_current_level_input_system(
    mut game_state_timer: ResMut<GameStateTimer>,
    keyboard_input: Res<Input<KeyCode>>,
    mut game_state: ResMut<State<GameState>>,
    mut next_game_state: ResMut<State<NextGameState>>,
    won: Res<Won>,
    score: Res<Score>,
    mut current_best: ResMut<CurrentBestScore>,
    mut highscore: ResMut<HighScore>,
    level_select: Res<State<LevelSelect>>,
) {
    if game_state_timer.0.elapsed_secs() < INTERACTION_DELAY {
        return ()
    }
    if keyboard_input.pressed(KeyCode::R) {
        match game_state.current() {
            GameState::Game => {
                game_state.set(GameState::Switch).unwrap();
                next_game_state.set(NextGameState::Game).unwrap();
                game_state_timer.0.reset();
                if current_best.0 < score.0 && won.0 {
                    current_best.0 = score.0;
                    let &LevelSelect::Level(current_level) = level_select.current();
                    highscore.0.entry(current_level)
                        .and_modify(|e|
                            if e < &mut current_best.0 {*e = current_best.0}
                        )
                        .or_insert(current_best.0);
                }
            }
            _ => {}
        }
    }
}

fn toggle_dev_mode_input_system(
    mut commands: Commands,
    keyboard_input: Res<Input<KeyCode>>,
    mut meshes: ResMut<Assets<Mesh>>,
    colors: Res<ColorHandle>,
    mut shoot_res: ResMut<ShootRes>,
    query: Query<Entity, With<DevModeIndicator>>,
    mut game_state_timer: ResMut<GameStateTimer>,
) {
    if game_state_timer.0.elapsed_secs() < INTERACTION_DELAY {
        return ()
    }
    if keyboard_input.pressed(KeyCode::D) {
        shoot_res.dev_mode = !shoot_res.dev_mode;
        if shoot_res.dev_mode {
            commands.spawn()
                .insert_bundle(MaterialMesh2dBundle {
                    mesh: meshes.add(Mesh::from(shape::Capsule{depth: 0.0, radius: 15.0, ..default()})).into(),
                    transform: Transform {
                        translation: Vec2::from_array([WINDOW_WIDTH, WINDOW_HEIGHT]).extend(0.0)/2.0,
                        ..default()
                    },
                    material: colors.dev_indicator.clone(),
                    ..default()
                })
                .insert(DevModeIndicator);
            game_state_timer.0.reset();
        } else {
            let entity = query.single();
            commands.entity(entity).despawn();
            game_state_timer.0.reset();
        }
    }
}

/*
    Plugins
*/

// labels
#[derive(Debug, Clone, PartialEq, Eq, Hash, SystemLabel)]
pub struct InputSystemsLabel;

pub struct InputHandlingPlugin;

impl Plugin for InputHandlingPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set(
            SystemSet::new()
                .label(InputSystemsLabel)
                .with_system(exit_current_state_input_system)
                .with_system(replay_current_level_input_system)
                .with_system(toggle_dev_mode_input_system)
        );
    }
}
