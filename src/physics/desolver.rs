
use bevy::prelude::*;

use ode_solvers::*;

use crate::{
    components::sources::Source,
    physics::physics::{Physics, PhysicsConstants},
};

type State = Vector4<f32>;
type Time = f64;

pub struct ODESolver<'world, 'state, 'query, 'solver> {
    physics: Physics,
    physics_constants: &'solver PhysicsConstants,
    sources: Query<'world, 'state, &'query Source>,
}

impl<'world, 'state, 'query, 'solver> ODESolver<'world, 'state, 'query, 'solver> {
    pub fn new_schwarzschild(sources: Query<'world, 'state, &'query Source>, physics_constants: &'solver PhysicsConstants) -> Self {
        Self {
            physics: Physics::Schwarzschild,
            physics_constants: physics_constants,
            sources: sources,
        }
    }
    pub fn new_newton(sources: Query<'world, 'state, &'query Source>, physics_constants: &'solver PhysicsConstants, dimension: f32) -> Self {
        Self {
            physics: Physics::Newton(dimension),
            physics_constants: physics_constants,
            sources: sources,
        }
    }

    fn newton_system(&self, &source: &Source, &probe_pos: &Vec2, probe_acc: &mut Vec2, &d: &f32) {
        let pos = probe_pos - source.pos;
        let r: f32 = pos.length();
        let r_hat = pos/r;
        if r > source.radius {
            *probe_acc += -self.physics_constants.G*source.mass/r.powf(d-1.0)*r_hat;
        }
    }

    fn schwarzschild_system(&self, &source: &Source, &probe_pos: &Vec2, &probe_vel: &Vec2, probe_acc: &mut Vec2) {
        let pos = probe_pos - source.pos;
        let rs: f32 = 2.0*self.physics_constants.G*source.mass/self.physics_constants.c.powi(2);
        let mut r: f32 = (pos[0]*pos[0] + pos[1]*pos[1]).sqrt();
        let diff = r-rs;
        if diff < 0.0 {
            return ();
        }
        if r < source.radius {
            r = source.radius;
        }
        probe_acc[0] += -rs/(2.0*diff*(r.powi(5))+1.0e-6)*(
            self.physics_constants.c.powi(2)*diff.powi(2)*r*pos[0]
            - pos[0]*(3.0*r*pos[0].powi(2) + 2.0*rs*pos[1].powi(2))*probe_vel[0].powi(2)
            - 2.0*pos[1]*(2.0*(2.0*r-rs)*pos[0].powi(2)+r*pos[1].powi(2))*probe_vel[0]*probe_vel[1]
            - pos[0]*(r*pos[1].powi(2) - 2.0*diff*pos[0].powi(2))*probe_vel[1].powi(2)
        );
        probe_acc[1] += -rs/(2.0*diff*r.powf(5.0)+1.0e-6)*(
            self.physics_constants.c.powi(2)*diff.powi(2)*r*pos[1]
            - pos[1]*(3.0*r*pos[1].powi(2) + 2.0*rs*pos[0].powi(2))*probe_vel[1].powi(2)
            - 2.0*pos[0]*(2.0*(2.0*r-rs)*pos[1].powi(2)+r*pos[0].powi(2))*probe_vel[0]*probe_vel[1]
            - pos[1]*(r*pos[0].powi(2) - 2.0*diff*pos[1].powi(2))*probe_vel[0].powi(2)
        );
    }
}

impl ode_solvers::System<State> for ODESolver<'_, '_, '_, '_> {
    fn system(&self, _t: Time, y: &State, dy: &mut State) {
        let probe_pos = Vec2::new(y[0], y[1]);
        let probe_vel = Vec2::new(y[2], y[3]);
        let mut probe_acc = Vec2::ZERO;
        for source in self.sources.iter() {
            match self.physics {
                Physics::Schwarzschild => self.schwarzschild_system(&source, &probe_pos, &probe_vel, &mut probe_acc),
                Physics::Newton(d) => self.newton_system(&source, &probe_pos, &mut probe_acc, &d)
            }
        }
        dy[0] = probe_vel[0];
        dy[1] = probe_vel[1];
        dy[2] = probe_acc[0];
        dy[3] = probe_acc[1];
    }
}
