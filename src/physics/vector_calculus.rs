use::bevy::prelude::Vec3;

pub trait Polar {
    fn polar_to_cartesian(&mut self, d: &f32) -> Self;
}

pub trait Cylindrical {
    fn cylindrical_to_cartesian(&mut self, d: &f32) -> Self;
}

pub trait Projectable {
    fn projected_position(&self) -> Vec3;
}

impl Polar for Vec3 {
    fn polar_to_cartesian(&mut self, d: &f32) -> Self {
        (self[0], self[1], self[2]) = (
            self[0].powf(1./d) * self[1].sin() * self[2].cos(),
            self[0].powf(1./d) * self[1].sin() * self[2].sin(),
            self[0].powf(1./d) * self[1].cos(),
        );
        *self
    }
}

impl Cylindrical for Vec3 {
    fn cylindrical_to_cartesian(&mut self, d: &f32) -> Self {
        (self[0], self[1], self[2]) = (
            self[0].powf(1./d) * self[1].cos(),
            self[0].powf(1./d) * self[1].sin(),
            self[2],
        );
        *self
    }
}

impl Projectable for Vec3 {
    fn projected_position(&self) -> Vec3 {
        let r_hat = self.normalize();
        let min_dist: f32 = 350.0;
        let u = min_dist/r_hat[0];
        let ret = Vec3::new(u*r_hat[1], u*r_hat[2], -1.1) * Vec3::new(1.5, 1.5, 1.0);
        ret
    }
}
