
use bevy::prelude::*;

#[derive(Component)]
pub struct Velocity {
    pub vec: Vec2
}

#[derive(Component)]
pub struct Radius(pub f32);
