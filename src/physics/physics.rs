use bevy::{
    prelude::*,
    time::FixedTimestep,
};

use crate::{
    components::{
        probe::{Probe,ShootRes,ProbeUpdateLabel},
        sources::{Source,SourceUpdateLabel},
        goal::Goal,
    },
    physics::general::{Velocity, Radius},
    mechanics::{
        gameover::{Won, Lost},
        general::{GameState, TIME_STEP}
    },
    levels::levels::LevelData,
};

/*
    Constants
*/

const DEFAULT_G: f32 = 200.0;
const DEFAULT_C: f32 = 300.0;
const DEFAULT_MEAN_R: f32 = 100.0;
const DEFAULT_DELTA_T: f32 = 0.5;

/*
    Resources
*/
pub struct Collided(pub bool);

/*
    Structures -- Physics data for levels
*/

struct ProbeUpdater {
    position: Vec2,
    velocity: Vec2,
}

pub enum CollisionPhysics {
    Crash,
    Bounce(f32)
}

impl CollisionPhysics {
    fn collide(
        &self,
        probe: &Probe,
        probe_velocity: &Velocity,
        target_pos: &Vec2,
        target_radius: &f32,
    ) -> Option<ProbeUpdater> {
        match self {
            CollisionPhysics::Crash => {
                CollisionPhysics::calculate_crash_collision(
                    probe,
                    probe_velocity,
                    target_pos,
                    target_radius,
                )
            }
            CollisionPhysics::Bounce(coefficient) => {
                CollisionPhysics::calculate_bounce_collision(
                    probe,
                    probe_velocity,
                    target_pos,
                    target_radius,
                    coefficient
                )
            }
        }
    }
    fn calculate_crash_collision(
        probe: &Probe,
        probe_velocity: &Velocity,
        target_pos: &Vec2,
        target_radius: &f32,
    ) -> Option<ProbeUpdater> {
        #[allow(non_snake_case)]
        let R = probe.radius + target_radius;
        let d = probe.pos - *target_pos;
        if d.length() < R {
            // proper collision
            let b = 2.0*probe_velocity.vec.normalize().dot(d);
            let c = d.dot(d) - R.powf(2.0);
            let alpha = b/2.0 + f32::sqrt(b.powf(2.0) - 4.0*c)/2.0;
            return Some(
                ProbeUpdater {
                    position: probe.pos - probe_velocity.vec.normalize()*alpha,
                    velocity: Vec2::NAN,
                }
            );
        }
        let q = probe.pos - probe.old_pos;
        let q_hat = q.normalize();
        let delta = d - (d.dot(q_hat))*q_hat;
        if delta.length() < R {
            // potential hit, probe path intersects with target
            let t = -d.dot(q_hat);
            if 0.0 < t && t < q.length() {
                // actual hit
                let t_tilde = (R.powf(2.0) + d.dot(q_hat).powf(2.0) - d.dot(d)).sqrt();
                return Some(
                    ProbeUpdater {
                        position: probe.pos + (t-t_tilde)*q_hat,
                        velocity: Vec2::NAN,
                    }
                );
            }
        }
        return None;
    }
    fn calculate_bounce_collision(
        probe: &Probe,
        probe_velocity: &Velocity,
        target_pos: &Vec2,
        target_radius: &f32,
        coefficient: &f32,
    ) -> Option<ProbeUpdater> {
        #[allow(non_snake_case)]
        let R = probe.radius + target_radius;
        let d = probe.pos - *target_pos;
        let q = probe.pos - probe.old_pos;
        let q_hat = q.normalize();
        let delta = d - (d.dot(q_hat))*q_hat;
        if delta.length() < R {
            // potential hit, probe path intersects with target
            let t = -d.dot(q_hat);
            if 0.0 < t && t < q.length() {
                // actual hit
                let t_tilde = (R.powf(2.0) + d.dot(q_hat).powf(2.0) - d.dot(d)).sqrt();
                let reflection_normal = ((probe.pos + (t-t_tilde)*q_hat) - *target_pos).normalize();
                let reflected_position = (probe.pos - 2.0*(probe.pos.dot(reflection_normal))*reflection_normal).normalize()*(q.length() - t_tilde) * *coefficient;
                let reflected_velocity = probe_velocity.vec - 2.0*(probe_velocity.vec.dot(reflection_normal))*reflection_normal;
                return Some(
                    ProbeUpdater {
                        position: reflected_position,
                        velocity: reflected_velocity * *coefficient,
                    }
                )
            }
        }
        return None;
    }
}

pub enum Topology {
    Open,
    Boxed(f32),
    Torus,
    Klein,
}

#[derive(PartialEq)]
pub enum Physics {
    Schwarzschild,
    Newton(f32),
}

#[allow(non_snake_case)]
pub struct PhysicsConstants {
    pub G: f32,
    pub c: f32,
    pub dt: f32,
}

impl PhysicsConstants {
    /// Reasonable default values for ordinary Schwarzschild physics
    /// of M~500 and black holes of M~5000, r~3.0
    pub fn default() -> Self {
        Self {
            G: DEFAULT_G,
            c: DEFAULT_C,
            dt: DEFAULT_DELTA_T,
        }
    }
    pub fn default_newton(d: f32) -> Self {
        Self {
            G: DEFAULT_G*DEFAULT_MEAN_R.powf(d-3.0),
            c: DEFAULT_C,
            dt: DEFAULT_DELTA_T,
        }
    }
}

/*
    Systems
*/

fn source_collisions(
        mut collided: ResMut<Collided>,
        mut shoot_res: ResMut<ShootRes>,
        mut lost: ResMut<Lost>,
        mut probe_query: Query<(&mut Probe, &mut Velocity), With<Probe>>,
        source_query: Query<&Source, With<Source>>,
        level_data: Res<LevelData>,
    ) {
    if probe_query.is_empty() ||
        collided.0 ||
        shoot_res.probe_freeze ||
        source_query.is_empty() {
        return ();
    }
    let (mut probe, mut velocity) = probe_query.single_mut();
    for source in source_query.iter() {
        match level_data.collision_physics.collide(&probe, velocity.as_ref(), &source.pos, &source.radius) {
            Some(updater) => {
                match level_data.collision_physics {
                    CollisionPhysics::Crash => {
                        collided.0 = true;
                        shoot_res.probe_freeze = true;
                        lost.0 = true;
                    },
                    _ => {}
                }
                probe.pos = updater.position.clone();
                if !updater.velocity.is_nan() {
                    velocity.vec = updater.velocity
                }
                return ();
            },
            None => {}
        }
    }
}

fn goal_collisions(
        mut won: ResMut<Won>,
        mut shoot_res: ResMut<ShootRes>,
        mut probe_query: Query<(&mut Probe, &Velocity), With<Probe>>,
        goal_query: Query<(&Transform, &Radius), (With<Goal>, Without<Probe>)>,
    ) {
    if probe_query.is_empty() {
        return ();
    }
    if goal_query.is_empty() {
        return ();
    }
    let (mut probe, velocity) = probe_query.single_mut();
    let (goal_transform, goal_radius) = goal_query.single();
    match CollisionPhysics::calculate_crash_collision(&probe, velocity, &goal_transform.translation.truncate(), &goal_radius.0) {
        Some(updater) => {
            shoot_res.probe_freeze = true;
            won.0 = true;
            probe.pos = updater.position.clone();
            return ();
        },
        None => {}
    }
}

/*
    Plugins
*/

// labels
#[derive(Debug, Clone, PartialEq, Eq, Hash, SystemLabel)]
pub struct PhysicsUpdateLabel;

pub struct PhysicsInteractionPlugin;

impl Plugin for PhysicsInteractionPlugin {
    fn build(&self, app: &mut App) {
        // Resources
        app.insert_resource(Collided(false));
        // Systems
        app.add_system_set(
            SystemSet::on_update(GameState::Game)
                .with_run_criteria(FixedTimestep::step(TIME_STEP as f64))
                .label(PhysicsUpdateLabel)
                .after(ProbeUpdateLabel)
                .after(SourceUpdateLabel)
                .with_system(source_collisions)
                .with_system(goal_collisions)
            );
    }
}
