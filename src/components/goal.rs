
use bevy::{
    prelude::*,
    sprite::MaterialMesh2dBundle,
};

use crate::{
    physics::general::Radius,
    mechanics::general::{CurrentLevel, GameState},
    levels::levels::LevelData,
    assets::assets::ColorHandle,
};

/*
    Components
*/

#[derive(Component)]
pub struct Goal;

/*
    Systems
*/

fn setup_goal(
        mut commands: Commands,
        mut meshes: ResMut<Assets<Mesh>>,
        colors: Res<ColorHandle>,
        level_data: Res<LevelData>,
    ) {
    let radius = Radius(level_data.goal.radius);
    commands.spawn()
        .insert_bundle(MaterialMesh2dBundle {
            mesh: meshes.add(Mesh::from(shape::Capsule{depth: 0.0, radius: radius.0, ..default()})).into(),
            transform: Transform {
                translation: level_data.goal.position.clone().extend(-1.0),
                ..default()
            },
            material: colors.goal.clone(),
            ..default()
        })
        .insert(Goal)
        .insert(radius)
        .insert(CurrentLevel);
}

/*
    Plugins
*/

pub struct GoalPlugin;

impl Plugin for GoalPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set(
            SystemSet::on_enter(GameState::Game)
                .with_system(setup_goal)
        );
    }
}
