
use bevy::{
    prelude::*,
    time::{Stopwatch, FixedTimestep},
    sprite::MaterialMesh2dBundle
};

use crate::{
    components::probe::{Probe, ShootRes, ProbeUpdateLabel},
    mechanics::general::{CurrentLevel, GameState, TIME_STEP},
    assets::assets::{ColorHandle, TracedotHandle},
    mechanics::gameover::BoxButtonAction,
};

use std::time::Duration;

/*
    Constants
*/

const TRACE_LIFETIME: f32 = 16.0;

/*
    Components
*/

#[derive(Component)]
struct TraceDot {
    timer: Stopwatch
}

/*
    Systems
*/

fn spawn_tracedot(
    mut commands: Commands,
    tracedot_handle: Res<TracedotHandle>,
    colors: Res<ColorHandle>,
    probe_query: Query<&Probe, With<Probe>>,
    shoot_res: Res<ShootRes>,
) {
    if shoot_res.probe_freeze || probe_query.is_empty() {
        return ()
    }
    let probe = probe_query.single();
    commands.spawn()
        .insert(TraceDot {timer: Stopwatch::new()})
        .insert_bundle(MaterialMesh2dBundle {
            mesh: tracedot_handle.0.clone().into(),
            transform: Transform {
                translation: probe.pos.extend(-1.0),
                scale: Vec3::splat(2.0),
                ..default()
            },
            material: colors.tracedot.clone(),
            ..default()
        })
        .insert(CurrentLevel);
}

fn tick_tracedot(
    mut query: Query<&mut TraceDot, With<TraceDot>>,
    interaction_query: Query<&BoxButtonAction, With<BoxButtonAction>>,
) {
    if !interaction_query.is_empty() {
        return ()
    }
    for mut tracedot in query.iter_mut() {
        tracedot.timer.tick(Duration::from_secs_f32(TIME_STEP));
    }
}

fn despawn_tracedot(
        mut commands: Commands,
        query: Query<(Entity, &TraceDot), With<TraceDot>>
    ) {
    for entity in query.iter()
            .filter(|x| {x.1.timer.elapsed_secs() > TRACE_LIFETIME})
            .map(|x| {x.0}) {
        commands.entity(entity).despawn();
    }
}

/*
    Plugins
*/

pub struct TracedotManagementPlugin;

impl Plugin for TracedotManagementPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set(
            SystemSet::on_update(GameState::Game)
                .with_run_criteria(FixedTimestep::step(TIME_STEP as f64))
                .after(ProbeUpdateLabel)
                .with_system(spawn_tracedot)
                .with_system(tick_tracedot.after(spawn_tracedot))
                .with_system(despawn_tracedot.after(tick_tracedot))
            );
    }
}
