
use std::ops::Rem;

use bevy::{
    prelude::*,
    sprite::MaterialMesh2dBundle,
    time::FixedTimestep,
};

use rand::Rng;
use rand_distr::{Normal, Distribution};

use crate::{
    mechanics::general::{TIME_STEP, CurrentLevel, GameState},
    levels::levels::LevelData,
    physics::{
        physics::Physics,
        vector_calculus::{Polar, Cylindrical, Projectable}
    },
};

/*
    Constants
*/

const BOX_SIZE: f32 = 1050.0;
const EXTRA_DIM_SIZE: f32 = 390000000.0*2.0;

/*
    Components
*/

#[derive(Component)]
pub struct BackgroundStar {
    pub actual_position: Vec3,
    pub absolute_magnitude: f32,
    pub apparent_magnitude: f32,
    extra_dim: f32,
    extra_dim_speed: f32,
}

/*
    Resources
*/

struct BackgroundRotation {
    quat: Quat
}

impl BackgroundRotation {
    pub fn new() -> BackgroundRotation {
        let mut rng = rand::thread_rng();
        let axis = Vec3::new(
            rng.gen_range(-1.0..1.0),
            rng.gen_range(-1.0..1.0),
            rng.gen_range(-1.0..1.0),
        ).normalize();
        BackgroundRotation {
            quat: Quat::from_axis_angle(axis, 0.05*TIME_STEP)
        }
    }
    pub fn new_axis(&mut self, &d: &f32) {
        let mut rng = rand::thread_rng();
        let axis = if d < 3.0 {
            Vec3::Z
        } else {
            Vec3::new(
                rng.gen_range(-1.0..1.0),
                rng.gen_range(-1.0..1.0),
                rng.gen_range(-1.0..1.0),
            ).normalize()
        };
        self.quat = Quat::from_axis_angle(axis, 0.05*TIME_STEP)
    }
}

impl BackgroundStar {
    fn new_spherical(d: &f32) -> BackgroundStar {
        let mut rng = rand::thread_rng();
        let actual_position = Vec3::new(
            rng.gen_range((BOX_SIZE.powf(*d)/10.0)..BOX_SIZE.powf(*d)), // r-values
            f32::acos(1.0-2.0*rng.gen::<f32>()),                        // theta-values
            rng.gen_range(0.0..(2.0*std::f32::consts::PI))              // phi-values
        ).polar_to_cartesian(d);
        let mag = BackgroundStar::get_new_magnitude(d);
        BackgroundStar {
            actual_position: actual_position,
            absolute_magnitude: mag,
            apparent_magnitude: mag/actual_position.length().powf(d-1.0),
            extra_dim: if *d > 3.0 {rng.gen_range(0.0..EXTRA_DIM_SIZE)} else {f32::INFINITY},
            extra_dim_speed: rng.gen_range((EXTRA_DIM_SIZE/1000.0/2.0)..(EXTRA_DIM_SIZE/1000.0))
        }
    }
    fn new_cylindrical(d: &f32) -> BackgroundStar {
        let mut rng = rand::thread_rng();
        let actual_position = Vec3::new(
            rng.gen_range((BOX_SIZE.powf(*d)/10.0)..BOX_SIZE.powf(*d)), // r-values
            rng.gen_range(0.0..(2.0*std::f32::consts::PI)),             // phi-values
            rng.gen_range((-BOX_SIZE/2.0)..(BOX_SIZE/2.0))              // z-values
        ).cylindrical_to_cartesian(d);
        let mag = BackgroundStar::get_new_magnitude(d);
        BackgroundStar {
            actual_position: actual_position,
            absolute_magnitude: mag,
            apparent_magnitude: mag/actual_position.length().powf(d-1.0),
            extra_dim: if *d > 3.0 {rng.gen_range(0.0..EXTRA_DIM_SIZE)} else {f32::INFINITY},
            extra_dim_speed: rng.gen_range((EXTRA_DIM_SIZE/1000.0/2.0)..(EXTRA_DIM_SIZE/1000.0))
        }
    }
    fn get_new_magnitude(d: &f32) -> f32 {
        let mut rng = rand::thread_rng();
        let mut mag = 3.0_f32.powf(d-3.0).min(1.0);
        let normal = Normal::new(mag, mag/1000.0).unwrap();
        mag = normal.sample(&mut rng);
        mag *= 10.0*(2.9_f32*100.0).powf(d-1.0) * if *d > 3.0 {(1.0 + (d-3.0)).powf(0.7)} else {1.0};
        if *d > 3.0 {
            return mag.min(EXTRA_DIM_SIZE)
        } else {
            return mag
        }
    }
}

/*
    Systems
*/

fn setup_background(
    mut commands: Commands,
    mut background_rotation: ResMut<BackgroundRotation>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    level_data: Res<LevelData>,
) {
    let d = match level_data.physics {
        Physics::Schwarzschild => 3.0,
        Physics::Newton(d) => d,
    };
    background_rotation.new_axis(&d);
    let mut rng = rand::thread_rng();
    let n_stars = 200+((100.0_f32*(d-1.0)) as i32)+ if d > 3.0 {500} else {0};
    for _ in 0..n_stars {
        let star = if d < 3.0 {
            BackgroundStar::new_cylindrical(&d)
        } else {
            BackgroundStar::new_spherical(&d)
        };
        if star.apparent_magnitude < 1.0 {
            // if it's not visible, bail out
            continue
        }
        let normal = Normal::new(0.2, 0.1).unwrap();
        let shift: f32 = normal.sample(&mut rng);
        let color = if rng.gen::<f32>() > 0.4_f32 {
            Color::rgb(1.0, 1.0, 1.0-shift.max(0.0))
        } else {
            if rng.gen::<f32>() > 0.5_f32 {
                Color::rgb(1.0+shift.max(0.0), 1.0, 1.0)*(1.0/(1.0+shift.max(0.0)))
            } else {
                Color::rgb(1.0, 1.0, 1.0+shift.max(0.0))*(1.0/(1.0+shift.max(0.0)))
            }
        };
        commands.spawn()
            .insert_bundle(
                MaterialMesh2dBundle {
                    mesh: meshes.add(Mesh::from(
                        shape::Capsule{
                            depth: 0.0,
                            radius: star.apparent_magnitude,
                            ..default()
                        })).into(),
                    transform: Transform {
                        translation: star.actual_position.projected_position(),
                        ..default()
                    },
                    material: materials.add(ColorMaterial::from(color)),
                    ..default()
                }
            )
            .insert(star)
            .insert(CurrentLevel);
    }
}

fn update_background(mut query: Query<(&mut BackgroundStar, &mut Transform), With<BackgroundStar>>, rotation: Res<BackgroundRotation>) {
    for (mut star, mut trans) in query.iter_mut() {
        star.actual_position = rotation.quat.mul_vec3(star.actual_position);
        trans.translation = star.actual_position.projected_position();
        if star.extra_dim < f32::INFINITY {
            star.extra_dim = (star.extra_dim + star.extra_dim_speed).rem(EXTRA_DIM_SIZE);
            if EXTRA_DIM_SIZE/2.0 - star.absolute_magnitude < star.extra_dim && EXTRA_DIM_SIZE/2.0 + star.absolute_magnitude > star.extra_dim {
                trans.scale = Vec3::ONE * (
                    (((-(EXTRA_DIM_SIZE/2.0 - star.absolute_magnitude) + star.extra_dim)/(2.0*star.absolute_magnitude))
                        * std::f32::consts::PI)
                        .sin()
                );
            } else {
                trans.scale = Vec3::ZERO;
            }
        }
    }
}

/*
    Plugins
*/

pub struct BackgroundHandlingPlugin;

impl Plugin for BackgroundHandlingPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(BackgroundRotation::new());
        // Game
        app
            .add_system_set(
                SystemSet::on_enter(GameState::Game)
                    .with_system(setup_background)
            )
            .add_system_set(
                SystemSet::on_update(GameState::Game)
                    .with_run_criteria(FixedTimestep::step(TIME_STEP as f64))
                    .with_system(update_background)
            );
        // Menu
        app
            .add_system_set(
                SystemSet::on_enter(GameState::Menu)
                    .with_system(setup_background)
            )
            .add_system_set(
                SystemSet::on_update(GameState::Menu)
                    .with_run_criteria(FixedTimestep::step(TIME_STEP as f64))
                    .with_system(update_background)
            );
    }
}
