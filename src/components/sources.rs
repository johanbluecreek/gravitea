use bevy::{
    prelude::*,
    sprite::MaterialMesh2dBundle,
    time::FixedTimestep,
};

use itertools::iproduct;

use crate::{
    components::probe::ProbeUpdateLabel,
    physics::physics::{Topology, Physics},
    mechanics::general::{
        CurrentLevel, GameStateTimer, GameState,
        WINDOW_HEIGHT, WINDOW_WIDTH, TIME_STEP,
    },
    levels::levels::LevelData,
    assets::assets::ColorHandle,
};

/*
    Components
*/

#[derive(Component, Copy, Clone)]
pub struct Source {
    pub pos: Vec2,
    pub radius: f32,
    pub mass: f32,
    pub movement: fn(Vec2, f32) -> Vec2,
    pub black_hole: bool,
}

impl Source {
    fn get_mirror_coord(&self) -> Vec2 {
        return Vec2::new((self.pos[0]/WINDOW_WIDTH).round(), (self.pos[1]/WINDOW_HEIGHT).round())
    }
}

/*
    Systems
*/

fn setup_sources(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    colors: Res<ColorHandle>,
    level_data: Res<LevelData>,
) {
    let mut sources_to_spawn: Vec<Source> = vec![];
    for source_data in level_data.sources.iter() {
        match level_data.topology {
            Topology::Open | Topology::Boxed(_) => {
                sources_to_spawn.push(
                    Source {
                        pos: source_data.position.clone(),
                        radius: source_data.radius,
                        mass: source_data.mass,
                        movement: source_data.movement,
                        black_hole: 2.0*source_data.mass*level_data.physics_constants.G/level_data.physics_constants.c.powf(2.0) > source_data.radius,
                    }
                );
            }
            Topology::Torus => {
                for (xi, yi) in iproduct!(-2..=2,-2..=2) {
                    sources_to_spawn.push(
                        Source {
                            pos: source_data.position.clone() + Vec2::new((xi as f32)*WINDOW_WIDTH, (yi as f32)*WINDOW_HEIGHT),
                            radius: source_data.radius,
                            mass: source_data.mass,
                            movement: source_data.movement,
                            black_hole: 2.0*source_data.mass*level_data.physics_constants.G/level_data.physics_constants.c.powf(2.0) > source_data.radius,
                        }
                    );
                }
            }
            Topology::Klein => {
                for (xi, yi) in iproduct!(-2..=2,-2..=2) {
                    let v = Vec2::new(
                        if yi % 2 == 0 {source_data.position.x} else {-source_data.position.x},
                        source_data.position.y
                    );
                    sources_to_spawn.push(
                        Source {
                            pos: v + Vec2::new((xi as f32)*WINDOW_WIDTH, (yi as f32)*WINDOW_HEIGHT),
                            radius: source_data.radius,
                            mass: source_data.mass,
                            movement: source_data.movement,
                            black_hole: 2.0*source_data.mass*level_data.physics_constants.G/level_data.physics_constants.c.powf(2.0) > source_data.radius
                        }
                    );
                }
            }
        }
    }
    for source in sources_to_spawn.iter() {
        let mut cmd = commands.spawn();
        cmd.insert(*source)
            .insert_bundle(MaterialMesh2dBundle {
                mesh: meshes.add(Mesh::from(shape::Capsule{depth: 0.0, radius: source.radius, ..default()})).into(),
                transform: Transform {
                    translation: source.pos.extend(0.0),
                    ..default()
                },
                material: if source.mass > 0.0 {colors.positive_source.clone()} else {colors.negative_source.clone()},
                ..default()
            })
            .insert(CurrentLevel);
        if level_data.physics == Physics::Schwarzschild {
            let r = 2.0*source.mass*level_data.physics_constants.G/level_data.physics_constants.c.powf(2.0);
            if r > source.radius {
                cmd.with_children(|parent| {
                    parent.spawn_bundle(
                        MaterialMesh2dBundle {
                            mesh: meshes.add(Mesh::from(shape::Capsule{depth: 0.0, radius: r-2.0, ..default()})).into(),
                            transform: Transform {
                                translation: Vec2::ZERO.extend(-1.01),
                                ..default()
                            },
                            material: colors.blackhole_background.clone(),
                            ..default()
                        }
                    );
                })
                .with_children(|parent| {
                    parent.spawn_bundle(
                        MaterialMesh2dBundle {
                            mesh: meshes.add(Mesh::from(shape::Capsule{depth: 0.0, radius: r, ..default()})).into(),
                            transform: Transform {
                                translation: Vec2::ZERO.extend(-1.02),
                                ..default()
                            },
                            material: colors.horizon.clone(),
                            ..default()
                        }
                    );
                });
            }
        }
    }
}

fn update_sources(
    mut source_query: Query<(&mut Source, &mut Transform), With<Source>>,
    game_state_timer: Res<GameStateTimer>,
    level_data: Res<LevelData>,
) {
    for (mut source, mut transform) in source_query.iter_mut() {
        source.pos = match level_data.topology {
            Topology::Open | Topology::Boxed(_) | Topology::Torus => {
                (source.movement)(source.pos, game_state_timer.0.elapsed_secs())
            }
            Topology::Klein => {
                let mirror = if source.get_mirror_coord().y as i32 % 2 == 0 {Vec2::ONE} else {Vec2::new(-1.0, 1.0)};
                (source.movement)(
                    source.pos*mirror,
                    game_state_timer.0.elapsed_secs()
                )*mirror
            }
        };
        transform.translation = source.pos.extend(0.0);
    }
}

/*
    Plugins
*/

// labels
#[derive(Debug, Clone, PartialEq, Eq, Hash, SystemLabel)]
pub struct SourceUpdateLabel;

pub struct SourceManagementPlugin;

impl Plugin for SourceManagementPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set(
                SystemSet::on_enter(GameState::Game)
                    .with_system(setup_sources)
        );
        app.add_system_set(
            SystemSet::on_update(GameState::Game)
                .with_run_criteria(FixedTimestep::step(TIME_STEP as f64))
                .label(SourceUpdateLabel)
                .after(ProbeUpdateLabel)
                .with_system(update_sources)
        );
    }
}
