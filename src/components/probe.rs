use bevy::{
    prelude::*,
    sprite::MaterialMesh2dBundle,
    render::camera::RenderTarget,
    math::Quat,
    time::FixedTimestep,
};

use ode_solvers::*;

use crate::{
    components::sources::Source,
    physics::{
        desolver::ODESolver,
        physics::{Topology, Physics, PhysicsUpdateLabel},
        general::Velocity,
    },
    mechanics::{
        general::{
            GameState,
            GameCamera, ClickPosition, MousePosition, GameStateTimer, CurrentLevel,
            WINDOW_WIDTH, WINDOW_HEIGHT, TIME_STEP, INTERACTION_DELAY,
        },
        score::Score,
        gameover::Gameoverbox,
    },
    levels::levels::LevelData,
    assets::assets::{ColorHandle, FontHandle},
};

type State = Vector4<f32>;

/*
    Components
*/

#[derive(Component)]
struct PowerText;

#[derive(Component)]
struct Dragbar;

#[derive(Component)]
pub struct OldDragbar;

#[derive(Component)]
struct OOBText;

#[derive(Component)]
struct OOBbar;

#[derive(Component, Copy, Clone)]
pub struct Probe {
    pub pos: Vec2,
    pub old_pos: Vec2,
    pub radius: f32
}

/*
    Resources
*/

pub struct ShootRes {
    pub probe_freeze: bool,
    pub dev_mode: bool,
    can_shoot: bool,
}

/*
    Systems
*/

fn freeze_probe(mut shoot_res: ResMut<ShootRes>) {
    shoot_res.probe_freeze = true;
}

fn reload_shot(mut shoot_res: ResMut<ShootRes>) {
    shoot_res.can_shoot = true;
}

fn setup_probe(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    colors: Res<ColorHandle>,
    level_data: Res<LevelData>,
) {
    let probe = Probe {pos: level_data.probe.position.clone(), old_pos: level_data.probe.position.clone(), radius: level_data.probe.radius};
    commands.spawn()
        .insert(probe.clone())
        .insert_bundle(MaterialMesh2dBundle {
            mesh: meshes.add(Mesh::from(shape::Capsule{depth: 0.0, radius: probe.radius, ..default()})).into(),
            transform: Transform {
                translation: probe.pos.extend(0.0),
                ..default()
            },
            material: colors.probe.clone(),
            ..default()
        })
        .insert(Velocity{vec: Vec2::new(0.0, 0.0)})
        .insert(CurrentLevel);
}

fn calc_probe(
    mut probe_query: Query<(&mut Probe, &mut Velocity), With<Probe>>,
    source_query: Query<&Source>,
    mut score: ResMut<Score>,
    shoot_res: Res<ShootRes>,
    level_data: Res<LevelData>,
) {
    if shoot_res.probe_freeze || probe_query.is_empty() {
        return ()
    }
    let mut relativistic = false;
    // setup the DE
    let de = match level_data.physics {
        Physics::Schwarzschild => {
            relativistic = true;
            ODESolver::new_schwarzschild(source_query, &level_data.physics_constants)
        },
        Physics::Newton(d) => ODESolver::new_newton(source_query, &level_data.physics_constants, d),
    };

    // setup initial conditions
    let (mut probe, mut probe_velocity) = probe_query.single_mut();

    let y0 = State::new(
        probe.pos.x,
        probe.pos.y,
        probe_velocity.vec[0],
        probe_velocity.vec[1]
    );

    // solve DE
    let mut stepper = Dopri5::from_param(
        de,
        0.0,
        level_data.physics_constants.dt as f64,
        1.0e-5,
        y0,
        1.0e-4,
        1.0e-4,
        0.1,
        0.01,
        0.1,
        1.0,
        1.0,
        0.0,
        10000,
        1000,
        dop_shared::OutputType::Sparse
    );
    let res = stepper.integrate();
    match res {
        Ok(_stats) => {},
        Err(err) => {println!("ODE solver failed: \"{}\"; but don't worry about it.", err)}
    }
    let f = stepper.y_out()[stepper.y_out().len()-1];
    probe.old_pos.x = probe.pos.x;
    probe.old_pos.y = probe.pos.y;
    probe.pos.x = f[0] as f32;
    probe.pos.y = f[1] as f32;
    probe_velocity.vec[0] = f[2] as f32;
    probe_velocity.vec[1] = f[3] as f32;
    if probe_velocity.vec.length() > level_data.physics_constants.c && relativistic {
        probe_velocity.vec = probe_velocity.vec/probe_velocity.vec.length()*level_data.physics_constants.c;
    }
    for i in 0..(stepper.y_out().len()-1) {
        let (dx, dy) = (stepper.y_out()[i][0] - stepper.y_out()[i+1][0], stepper.y_out()[i][1] - stepper.y_out()[i+1][1]);
        score.0 += f32::sqrt(dx.powi(2) + dy.powi(2))/100.0;
    }
}

fn update_probe(
    mut query: Query<(&mut Velocity, &mut Probe), With<Probe>>,
    level_data: Res<LevelData>,
) {
    if query.is_empty() {
        // TODO: If this system is part of on_update GameState::Game
        // then it should not be executed when GameState::Menu is
        // active. BUT! For some reason, it still appears to run
        // in GameState::Menu. Figure out why.
        return ()
    }
    let (mut velocity, mut probe) = query.single_mut();
    // calc_probe updates pos & old_pos
    // here we should:
    //  * wrap pos according to topology
    //  * if we wrapped
    //      - old_pos <- pos (avoid inferred collision with mirror images)
    let mut wrapped = false;
    match level_data.topology {
        Topology::Open => {},
        Topology::Boxed(r) => {
            if probe.pos.x > WINDOW_WIDTH/2.0 {
                let diff = WINDOW_WIDTH/2.0 - probe.pos.x;
                probe.pos.x += 2.0*diff;
                velocity.vec *= Vec2::new(-1.0, 1.0)*r;
            } else if probe.pos.x < -WINDOW_WIDTH/2.0 {
                let diff = -WINDOW_WIDTH/2.0 - probe.pos.x;
                probe.pos.x += 2.0*diff;
                velocity.vec *= Vec2::new(-1.0, 1.0)*r;
            }
            if probe.pos.y > WINDOW_HEIGHT/2.0 {
                let diff = WINDOW_HEIGHT/2.0 - probe.pos.y;
                probe.pos.y += 2.0*diff;
                velocity.vec *= Vec2::new(1.0, -1.0)*r;
            } else if probe.pos.y < -WINDOW_HEIGHT/2.0 {
                let diff = -WINDOW_HEIGHT/2.0 - probe.pos.y;
                probe.pos.y += 2.0*diff;
                velocity.vec *= Vec2::new(1.0, -1.0)*r;
            }
        }
        Topology::Torus => {
            if probe.pos.x > WINDOW_WIDTH/2.0 {
                let diff = WINDOW_WIDTH/2.0 - probe.pos.x;
                probe.pos.x = -WINDOW_WIDTH/2.0 + diff;
                wrapped = true;
            } else if probe.pos.x < -WINDOW_WIDTH/2.0 {
                let diff = -WINDOW_WIDTH/2.0 - probe.pos.x;
                probe.pos.x = WINDOW_WIDTH/2.0 + diff;
                wrapped = true;
            }
            if probe.pos.y > WINDOW_HEIGHT/2.0 {
                let diff = WINDOW_HEIGHT/2.0 - probe.pos.y;
                probe.pos.y = -WINDOW_HEIGHT/2.0 + diff;
                wrapped = true;
            } else if probe.pos.y < -WINDOW_HEIGHT/2.0 {
                let diff = -WINDOW_HEIGHT/2.0 - probe.pos.y;
                probe.pos.y = WINDOW_HEIGHT/2.0 + diff;
                wrapped = true;
            }
        }
        Topology::Klein => {
            if probe.pos.x > WINDOW_WIDTH/2.0 {
                let diff = WINDOW_WIDTH/2.0 - probe.pos.x;
                probe.pos.x = -WINDOW_WIDTH/2.0 + diff;
                wrapped = true;
            } else if probe.pos.x < -WINDOW_WIDTH/2.0 {
                let diff = -WINDOW_WIDTH/2.0 - probe.pos.x;
                probe.pos.x = WINDOW_WIDTH/2.0 + diff;
                wrapped = true;
            }
            if probe.pos.y > WINDOW_HEIGHT/2.0 {
                let diff = WINDOW_HEIGHT/2.0 - probe.pos.y;
                probe.pos.y = -WINDOW_HEIGHT/2.0 + diff;
                probe.pos.x = -probe.pos.x;
                velocity.vec *= Vec2::new(-1.0, 1.0);
                wrapped = true;
            } else if probe.pos.y < -WINDOW_HEIGHT/2.0 {
                let diff = -WINDOW_HEIGHT/2.0 - probe.pos.y;
                probe.pos.y = WINDOW_HEIGHT/2.0 + diff;
                probe.pos.x = -probe.pos.x;
                velocity.vec *= Vec2::new(-1.0, 1.0);
                wrapped = true;
            }
        }
    }
    if wrapped {
        probe.old_pos = probe.pos;
    }
}

fn push_probe(
        mouse: Res<Input<MouseButton>>,
        click: Res<ClickPosition>,
        mouse_pos: Res<MousePosition>,
        mut probe_query: Query<&mut Velocity, With<Probe>>,
        mut commands: Commands,
        entity_query: Query<Entity, With<Dragbar>>,
        mut old_dragbar_query: Query<(&mut Handle<ColorMaterial>, Entity), With<OldDragbar>>,
        mut meshes: ResMut<Assets<Mesh>>,
        colors: Res<ColorHandle>,
        fonts: Res<FontHandle>,
        text_query: Query<Entity, With<PowerText>>,
        mut shoot_res: ResMut<ShootRes>,
        game_state_timer: Res<GameStateTimer>,
        level_data: Res<LevelData>,
        gameover_query: Query<&Gameoverbox, With<Gameoverbox>>,
    ) {
    if game_state_timer.0.elapsed_secs() < INTERACTION_DELAY ||
            !gameover_query.is_empty() ||
            if !shoot_res.dev_mode {!shoot_res.can_shoot} else {false}
            {
        return ()
    }

    let mut velocity = click.pos - mouse_pos.world_pos;
    let v_len = velocity.length();
    if v_len > 0.0 {
        velocity = velocity/v_len;
        velocity = velocity*v_len/(v_len+level_data.physics_constants.c.powf(1.5))*level_data.physics_constants.c*10.0;
    }

    if mouse.pressed(MouseButton::Left) {
        for entity in entity_query.iter() {
            commands.entity(entity).despawn();
        }
        let v = click.pos - mouse_pos.world_pos;
        commands.spawn()
            .insert_bundle(MaterialMesh2dBundle {
                mesh: meshes.add(Mesh::from(
                    shape::Quad{size: Vec2::new(f32::sqrt(v.dot(v)), 2.0), ..default()}
                    //shape::Plane{size: 1.0, ..default()}
                )).into(),
                transform: Transform {
                    translation: (click.pos-v/2.0).extend(0.0),
                    scale: Vec3::splat(1.0),
                    rotation: {
                        Quat::from_axis_angle(Vec3::Z, -v.angle_between(Vec2::X))
                    }
                },
                material: colors.dragbar.clone(),
                ..default()
            })
            .insert(Dragbar)
            .insert(CurrentLevel);
        for entity in text_query.iter() {
            commands.entity(entity).despawn();
        }
        commands.spawn()
            .insert_bundle(TextBundle {
                style: Style {
                    position_type: PositionType::Absolute,
                    position: UiRect {
                        bottom: Val::Px(mouse_pos.screen_pos[1]),
                        left: Val::Px(mouse_pos.screen_pos[0]),
                        ..default()
                    },
                    ..default()
                },
                text: Text {
                    sections: vec![
                        TextSection {
                            value: format!("{:.1}", velocity.length()/level_data.physics_constants.c*100.0),
                            style: TextStyle {
                                font: fonts.0.clone(),
                                font_size: 12.0,
                                color: Color::WHITE,
                            },
                        },
                    ],
                    ..default()
                },
                ..default()
            })
                .insert(PowerText)
                .insert(CurrentLevel);
    }

    if mouse.just_released(MouseButton::Left) {
        for entity in entity_query.iter() {
            commands.entity(entity).remove::<Dragbar>();
            commands.entity(entity).insert(OldDragbar);
            for (mut color_handle, oldbar_entity) in old_dragbar_query.iter_mut() {
                if *color_handle == colors.dragbar {
                    *color_handle = colors.dragbar_fade1.clone()
                } else if *color_handle == colors.dragbar_fade1 {
                    *color_handle = colors.dragbar_fade2.clone()
                } else if *color_handle == colors.dragbar_fade2 {
                    *color_handle = colors.dragbar_fade3.clone()
                } else if *color_handle == colors.dragbar_fade3 {
                    commands.entity(oldbar_entity).despawn();
                }
            }
        }
        for entity in text_query.iter() {
            commands.entity(entity).despawn();
        }
        let mut vel = probe_query.single_mut();
        vel.vec = velocity;
        shoot_res.probe_freeze = false;
        if !shoot_res.dev_mode {
            shoot_res.can_shoot = false;
        }
    }
}

fn draw_out_of_bounds(
        wnds: Res<Windows>,
        camera_query: Query<&Camera, With<GameCamera>>,
        probe_query: Query<&Probe, With<Probe>>,
        mut commands: Commands,
        text_query: Query<Entity, With<OOBText>>,
        entity_query: Query<Entity, With<OOBbar>>,
        mut meshes: ResMut<Assets<Mesh>>,
        colors: Res<ColorHandle>,
        fonts: Res<FontHandle>,
        level_data: Res<LevelData>,
    ) {
    match level_data.topology {
        Topology::Open => {
            let camera = camera_query.single();
            let wnd = if let RenderTarget::Window(id) = camera.target {
                wnds.get(id).unwrap()
            } else {
                wnds.get_primary().unwrap()
            };
            let window_size = Vec2::new(wnd.width() as f32, wnd.height() as f32);
            let probe = probe_query.single();
            let world_size = camera.projection_matrix().inverse()
                .project_point3(-Vec3::ONE)
                .truncate().abs();
            let bound: Vec2 = probe.pos / world_size;
            if bound.abs().max_element() > 1.0 {
                let mut screen_position: Vec2 = (((probe.pos / world_size) + Vec2::ONE)/2.0*window_size).clamp(Vec2::ZERO, window_size);
                if bound[0] > 1.0 {
                    screen_position += Vec2::new(-40.0, 0.0);
                }
                if bound[1] > 1.0 {
                    screen_position += Vec2::new(0.0, -15.0);
                }
                for entity in text_query.iter() {
                    commands.entity(entity).despawn()
                }
                for entity in entity_query.iter() {
                    commands.entity(entity).despawn()
                }
                commands.spawn()
                    .insert_bundle(TextBundle {
                        style: Style {
                            position_type: PositionType::Absolute,
                            position: UiRect {
                                bottom: Val::Px(screen_position[1]),
                                left: Val::Px(screen_position[0]),
                                ..default()
                            },
                            ..default()
                        },
                        text: Text {
                            sections: vec![
                                TextSection {
                                    value: format!("{:.1}", (probe.pos-probe.pos.clamp(-world_size,world_size)).length()),
                                    style: TextStyle {
                                        font: fonts.0.clone(),
                                        font_size: 12.0,
                                        color: Color::WHITE,
                                    },
                                },
                            ],
                            ..default()
                        },
                        ..default()
                    })
                    .insert(OOBText)
                    .insert(CurrentLevel);
                //
                commands.spawn()
                    .insert_bundle(MaterialMesh2dBundle {
                        mesh: meshes.add(Mesh::from(
                            shape::Quad{size: Vec2::new(((probe.pos-probe.pos.clamp(-world_size,world_size)).length()+1.0).ln()*10.0, 2.0), ..default       ()}
                            //shape::Plane{size: 1.0, ..default()}
                        )).into(),
                        transform: Transform {
                            translation: probe.pos.clamp(-world_size,world_size).extend(0.0),
                            scale: Vec3::splat(1.0),
                            rotation: {
                                Quat::from_axis_angle(Vec3::Z, -probe.pos.angle_between(Vec2::X))
                            }
                        },
                        material: colors.probe_oob.clone(),
                        ..default()
                    })
                    .insert(OOBbar)
                    .insert(CurrentLevel);
            } else {
                for entity in text_query.iter() {
                    commands.entity(entity).despawn()
                }
                for entity in entity_query.iter() {
                    commands.entity(entity).despawn()
                }
            }
        },
        _ => {}
    }
}

fn finalize_probe(
    mut probe_query: Query<(&Probe, &mut Transform), With<Probe>>,
) {
    if probe_query.is_empty() {
        return ();
    }
    let (probe, mut probe_transform) = probe_query.single_mut();
    probe_transform.translation = probe.pos.extend(0.0);
}

/*
    Plugin
*/

// labels
#[derive(Debug, Clone, PartialEq, Eq, Hash, SystemLabel)]
pub struct ProbeUpdateLabel;

pub struct ProbeManagementPlugin;

impl Plugin for ProbeManagementPlugin {
    fn build(&self, app: &mut App) {
        app
            .insert_resource(
                ShootRes {
                    probe_freeze: true,
                    dev_mode: false,
                    can_shoot: true
                }
            )
            .add_system_set(
                SystemSet::on_enter(GameState::Game)
                    .with_system(setup_probe)
            )
            .add_system_set(
                SystemSet::on_update(GameState::Game)
                    .with_system(push_probe)
                    .with_system(draw_out_of_bounds)
            )
            .add_system_set(
                SystemSet::on_exit(GameState::Game)
                    .with_system(freeze_probe)
                    .with_system(reload_shot)
            );
        app.add_system_set(
            SystemSet::on_update(GameState::Game)
                .with_run_criteria(FixedTimestep::step(TIME_STEP as f64))
                .label(ProbeUpdateLabel)
                .with_system(calc_probe)
                .with_system(update_probe.after(calc_probe))
        );
        app.add_system_set(
            SystemSet::on_update(GameState::Game)
                .with_run_criteria(FixedTimestep::step(TIME_STEP as f64))
                .after(PhysicsUpdateLabel)
                .with_system(finalize_probe)
            );
    }
}
