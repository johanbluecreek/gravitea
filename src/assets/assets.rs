
use bevy::prelude::*;

pub struct TracedotHandle(pub Handle<Mesh>);

pub struct ColorHandle {
    pub horizon: Handle<ColorMaterial>,
    pub blackhole_background: Handle<ColorMaterial>,
    pub positive_source: Handle<ColorMaterial>,
    pub negative_source: Handle<ColorMaterial>,
    pub tracedot: Handle<ColorMaterial>,
    pub border: Handle<ColorMaterial>,
    pub probe: Handle<ColorMaterial>,
    pub dragbar: Handle<ColorMaterial>,
    pub dragbar_fade1: Handle<ColorMaterial>,
    pub dragbar_fade2: Handle<ColorMaterial>,
    pub dragbar_fade3: Handle<ColorMaterial>,
    pub probe_oob: Handle<ColorMaterial>,
    pub goal: Handle<ColorMaterial>,
    pub dev_indicator: Handle<ColorMaterial>,
}

pub struct FontHandle(pub Handle<Font>);

fn tracedot_assets(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
) {
    commands.insert_resource(
        TracedotHandle(meshes.add(Mesh::from(shape::Capsule{depth: 0.0, ..default()})))
    );
}

fn color_assets(
    mut commands: Commands,
    mut materials: ResMut<Assets<ColorMaterial>>
) {
    let dragbar_color = Color::PURPLE;
    let mut dragbar_fade1_color = dragbar_color.clone();
    dragbar_fade1_color.set_a(0.75);
    let mut dragbar_fade2_color = dragbar_color.clone();
    dragbar_fade2_color.set_a(0.50);
    let mut dragbar_fade3_color = dragbar_color.clone();
    dragbar_fade3_color.set_a(0.25);
    commands.insert_resource(
        ColorHandle {
            horizon: materials.add(ColorMaterial::from(Color::WHITE)),
            blackhole_background: materials.add(ColorMaterial::from(Color::BLACK)),
            positive_source: materials.add(ColorMaterial::from(Color::BLUE)),
            negative_source: materials.add(ColorMaterial::from(Color::GREEN)),
            tracedot: materials.add(ColorMaterial::from(Color::WHITE)),
            border: materials.add(ColorMaterial::from(Color::ORANGE)),
            probe: materials.add(ColorMaterial::from(Color::RED)),
            dragbar: materials.add(ColorMaterial::from(dragbar_color)),
            dragbar_fade1: materials.add(ColorMaterial::from(dragbar_fade1_color)),
            dragbar_fade2: materials.add(ColorMaterial::from(dragbar_fade2_color)),
            dragbar_fade3: materials.add(ColorMaterial::from(dragbar_fade3_color)),
            probe_oob: materials.add(ColorMaterial::from(Color::RED)),
            goal: materials.add(ColorMaterial::from(Color::YELLOW)),
            dev_indicator: materials.add(ColorMaterial::from(Color::WHITE)),
        }
    );
}

fn font_assets(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
) {
    commands.insert_resource(
        FontHandle(asset_server.load("fonts/freesansbold.ttf"))
    );
}

pub struct AssetGenPlugin;

impl Plugin for AssetGenPlugin {
    fn build(&self, app: &mut App) {
        app
            .add_startup_system(tracedot_assets)
            .add_startup_system(color_assets)
            .add_startup_system(font_assets)
        ;
    }
}
