use bevy::{
    prelude::*,
};

use gravitea::{
    mechanics::{
        plugins::{
            DiagnosticsGroup,
            GraviteaPluginsGroup,
        },
    },
};

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_plugins(DiagnosticsGroup)
        .add_plugins(GraviteaPluginsGroup)
        .run();
}
