use bevy::prelude::*;

use crate::{
    components::probe::OldDragbar,
    physics::{
        physics::{Topology, Physics, PhysicsConstants, CollisionPhysics},
    },
    mechanics::general::{
        TIME_STEP,
        GameState,
        CurrentLevel
    },
};

/*
    Constants
*/

const TOTAL_NUM_LEVELS: i32 = 3;

/*
    States
*/

#[derive(Clone, Eq, PartialEq, Debug, Hash)]
pub enum LevelSelect {
    Level(i32),
}

impl LevelSelect {
    pub fn num_levels() -> i32 {
        return TOTAL_NUM_LEVELS
    }
    pub fn get_level_number(&self) -> &i32 {
        let LevelSelect::Level(i) = self;
        return i
    }
    pub fn get_name(&self) -> &str {
        match self.get_level_number() {
            &0 => "Black Hole",
            &1 => "The Wall",
            &2 => "The Orbit",
            _ => "DUMMY"
        }
    }
    pub fn get_level_data(&self) -> LevelData {
        match self.get_level_number() {
            &0 => {
                LevelData {
                    sources: vec![SourceData::new_static_manual(
                        Vec2::new(0.0, 100.0),
                        5000.0,
                        3.0,
                    )],
                    probe: ProbeData {position: Vec2::new(-500.0, 0.0), radius: 7.0},
                    goal: GoalData {position: Vec2::new(500.0, 0.0), radius: 12.0},
                    physics_constants: PhysicsConstants { G: 50.0, c: 150.0, dt: 0.3 },
                    physics: Physics::Schwarzschild,
                    topology: Topology::Open,
                    collision_physics: CollisionPhysics::Crash,
                }
            }
            &1 => {
                LevelData {
                    sources: vec![
                        SourceData::new_static(Vec2::new(0.0, -500.0), -50.0),
                        SourceData::new_static(Vec2::new(0.0, -450.0), -50.0),
                        SourceData::new_static(Vec2::new(0.0, -400.0), -50.0),
                        SourceData::new_static(Vec2::new(0.0, -350.0), -50.0),
                        SourceData::new_static(Vec2::new(0.0, -300.0), -50.0),
                        SourceData::new_static(Vec2::new(0.0, -250.0), -50.0),
                        SourceData::new_static(Vec2::new(0.0, -200.0), -50.0),
                        SourceData::new_static(Vec2::new(0.0, -150.0), -50.0),
                        SourceData::new_static(Vec2::new(0.0, -100.0), -50.0),
                        SourceData::new_static(Vec2::new(0.0, -50.0), -50.0),
                        SourceData::new_static(Vec2::new(0.0, 0.0), -50.0),
                        SourceData::new_static(Vec2::new(0.0, 50.0), -50.0),
                        SourceData::new_static(Vec2::new(0.0, 100.0), -50.0),
                        SourceData::new_static(Vec2::new(0.0, 150.0), -50.0),
                        SourceData::new_static(Vec2::new(0.0, 200.0), -50.0),
                        SourceData::new_static(Vec2::new(0.0, 250.0), -50.0),
                        SourceData::new_static(Vec2::new(0.0, 300.0), -50.0),
                        SourceData::new_static(Vec2::new(0.0, 350.0), -50.0),
                        SourceData::new_static(Vec2::new(0.0, 400.0), -50.0),
                        SourceData::new_static(Vec2::new(0.0, 450.0), -50.0),
                        SourceData::new_static(Vec2::new(0.0, 500.0), -50.0),
                    ],
                    probe: ProbeData {position: Vec2::new(-500.0, 0.0), radius: 7.0},
                    goal: GoalData {position: Vec2::new(500.0, 0.0), radius: 12.0},
                    physics_constants: PhysicsConstants::default(),
                    physics: Physics::Schwarzschild,
                    topology: Topology::Boxed(0.8),
                    collision_physics: CollisionPhysics::Crash,
                }
            }
            &2 => {
                LevelData {
                    sources: vec![
                        SourceData::new_moving(
                            Vec2::new(0.0, 0.0),
                            500.0,
                            |p: Vec2, t: f32| {p + 100.0*TIME_STEP*Vec2::new(t.cos(), -t.sin())}
                        ),
                    ],
                    probe: ProbeData {position: Vec2::new(-500.0, 0.0), radius: 7.0},
                    goal: GoalData {position: Vec2::new(500.0, 0.0), radius: 12.0},
                    physics_constants: PhysicsConstants::default_newton(2.0),
                    physics: Physics::Newton(2.0),
                    topology: Topology::Klein,
                    collision_physics: CollisionPhysics::Crash,
                }
            }
            _ => {
                DUMMY_LEVEL
            }
        }
    }
}

/*
    Resources
*/

pub struct LevelsEnded(pub bool);

pub struct SourceData {
    pub position: Vec2,
    pub radius: f32,
    pub mass: f32,
    pub movement: fn(Vec2, f32) -> Vec2,
}

impl SourceData {
    fn new_static(position: Vec2, mass: f32) -> Self {
        SourceData {
            position: position,
            radius: SourceData::radius_from_mass(&mass),
            mass: mass,
            movement: |position: Vec2, _time: f32| {position},
        }
    }
    fn new_moving(position: Vec2, mass: f32, movement: fn(Vec2, f32) -> Vec2) -> Self {
        SourceData {
            position: position,
            radius: SourceData::radius_from_mass(&mass),
            mass: mass,
            movement: movement,
        }
    }
    fn new_static_manual(position: Vec2, mass: f32, radius: f32) -> Self {
        SourceData {
            position: position,
            radius: radius,
            mass: mass,
            movement: |position: Vec2, _time: f32| {position},
        }
    }
    fn radius_from_mass(mass: &f32) -> f32 {
        (mass/1.2).abs().powf(1.0/3.0)
    }
}

pub struct ProbeData {
    pub position: Vec2,
    pub radius: f32,
}

pub struct GoalData {
    pub position: Vec2,
    pub radius: f32,
}

pub struct LevelData {
    pub sources: Vec<SourceData>,
    pub probe: ProbeData,
    pub goal: GoalData,
    pub physics: Physics,
    pub physics_constants: PhysicsConstants,
    pub topology: Topology,
    pub collision_physics: CollisionPhysics,
}

const DUMMY_LEVEL: LevelData = LevelData {
    sources: vec![],
    probe: ProbeData {position: Vec2::ZERO, radius: 0.0},
    goal: GoalData {position: Vec2::ZERO, radius: 0.0},
    physics: Physics::Schwarzschild,
    physics_constants: PhysicsConstants { G: 100.0, c: 300.0, dt: 1.3 },
    topology: Topology::Open,
    collision_physics: CollisionPhysics::Crash,
};

/*
    Systems
*/

pub fn level_setup(
    mut commands: Commands,
    level_select: Res<State<LevelSelect>>,
    mut level_end: ResMut<LevelsEnded>,
) {
    if level_select.current().get_level_number() == &(TOTAL_NUM_LEVELS-1) {
        level_end.0 = true
    }
    commands.remove_resource::<LevelData>();
    commands.insert_resource(level_select.current().get_level_data());
}

fn reset_level(
    mut level_end: ResMut<LevelsEnded>,
    mut level_select: ResMut<State<LevelSelect>>,
) {
    level_end.0 = false;
    match level_select.current() {
        LevelSelect::Level(i) => {
            if *i != 0 {
                level_select.set(LevelSelect::Level(0)).unwrap();
            }
        }
    }
}

fn despawn_current_level(
    mut commands: Commands,
    query: Query<Entity, With<CurrentLevel>>
) {
    for entity in query.iter() {
        commands.entity(entity).despawn_recursive();
    }
}

fn despawn_current_level_restart(
    mut commands: Commands,
    query: Query<Entity, (With<CurrentLevel>, Without<OldDragbar>)>,
) {
    for entity in query.iter() {
        commands.entity(entity).despawn_recursive();
    }
}

/*
    Plugins
*/

pub struct LevelManagementPlugin;

impl Plugin for LevelManagementPlugin {
    fn build(&self, app: &mut App) {
        // Resources
        app.insert_resource(DUMMY_LEVEL)
            .insert_resource(LevelsEnded(false));
        // States
        app.add_state(LevelSelect::Level(0));
        // Systems
        app.add_system_set(
                SystemSet::on_enter(GameState::Menu)
                    .with_system(despawn_current_level)
                    .with_system(reset_level.after(despawn_current_level))
            )
            .add_system_set(
                SystemSet::on_exit(GameState::Menu)
                    .with_system(level_setup)
            )
            .add_system_set(
                SystemSet::on_exit(GameState::Menu)
                    .with_system(despawn_current_level)
                    .with_system(level_setup)
            )
            .add_system_set(
                SystemSet::on_enter(GameState::Switch)
                    .with_system(despawn_current_level_restart)
            );
    }
}
