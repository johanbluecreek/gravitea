# Gravitea

## TODO/Roadmap

### ~~version 0.3.0~~

 * ~~Goals~~
   - ~~Add a way to win the current level~~
     * ~~Add win display~~
 * ~~Levels~~
   - ~~Add a second level~~
     * ~~Reset to first level when returning to menu~~
     * ~~Remove next button when on last level~~
   - ~~Level selection menu item~~
 * ~~Score~~
   - ~~Display high score at end of all levels~~

### ~~version 0.4.0~~

 * ~~Physics~~
   - ~~Make it possible to change physics per level~~
   - ~~Add Newtonian physics~~
   - ~~Make it possible to set Newtonian dimension~~
   - ~~Have the background change with physics~~
 * ~~Sources~~
   - ~~Look over if we really have to copy sources for physics calcs.~~
   - ~~Add mass to sources~~
   - ~~Have sources mass/radius scale in an appropriate way~~
 * ~~Sources and probes~~
   - ~~Remove the extra position vector?~~
 * ~~de-solving/winning~~
   - ~~Interpolate path to make sure at large velocities the probe can crash into sources and hit the goal~~
 * ~~Stars~~
   - ~~Are there really any blue stars?~~

### ~~version 0.5.0~~

  * ~~Physics~~
    - ~~Topologies~~
      * ~~Make a Klein-bottle level~~
      * ~~Make a torus level~~
    - ~~Boxed levels~~
      * ~~Levels where the probe cannot escape, but bounces on the edges~~
      * ~~How does that work with resizing of the window?~~

#### ~~version 0.5.1~~

 * ~~Convert to iyes-stateless~~ No, one cannot make modular plugins with `iyes-stateless`. See [this github issue](https://github.com/IyesGames/iyes_loopless/issues/38)
 * ~~Redesign plugins structure~~
   - ~~Have each file that defines systems collect them as Plugins.~~
     ~~This will avoid having large imports in `main.rs`, not all~~
     ~~systems needs to be public. I think it will be easier to~~
     ~~read too.~~
   - ~~In `mechanics/plugins.rs` we can still collect plugins into~~
     ~~plugin-groups that are passed to `main.rs`.~~

### version 0.6.0: Game mechanics

 * ~~Save high-scores to file~~
 * ~~Collision with source screen/game over~~
 * ~~Add restart keyboard shortcut (r)~~
 * Sources that you can bounce against
 * ~~Tune velocity given to probe in various dimensions (too easy to get ~99% velocity now)~~
 * ~~Display high-scores at level selection~~
 * ~~Implement names for levels~~
 * Implement achievements/goals for levels, e.g. 0-5 "stars", 1 star for every 100 score or whatever is appropriate for the level
 * Show level physics/topology by some UI-indicator
 * Inverse scoring mode. Mode where you have a max score which counts down.
 * ~~Menu item displaying controls for the user~~
 * ~~Check that scores are updated and displayed correctly when starting the game without high score file~~
 * ~~Have the tracedots not tick and despawn when the gameover box is shown.~~
 * Save the probe push direction line when released to be able to reproduce old run. Also save the high score?
 * ~~Convert all scores to E-notation.~~
 * ~~Implement a single attempt shot for normal game play (and a developer mode)~~

### version 0.A.0: Level designs

 * Earth and moon level (fixed source with static orbiting source)
 * Slalom level (an alley of sources leading to the goal)
 * Billiards (boxed level with bounce sources)
 * Drop it (some hard level, but where releasing the probe without any velocity gives you one star)
 * Klein bottle level
 * Torus level
 * 4d level
 * More levels to come...

### version 0.B.0: Graphics

 * Textures for...
   - probe (tea)
   - goal
   - Arrows (out of bounds, topology)
 * Optional textures for sources
   - An Earth source
   - Moon
   - Random Earth-likes
 * Switch from Capsule to Circle where appropriate

### version 0.C.0: Distributing the game

 * Figure out how to build portable executables for various platforms
 * Make releases on gitlab

### version 1.0.0: First complete game

This version should have:

 * Playable challenging levels with large variety of physics explored
 * Persistent high scores and achievements to encourage the user to play again and get better scores
 * The objects has some texture and are not just plain bevy meshes
 * Have a readme explaining how to download and play the game

### version 1.A.0: Learning

 * Add option to turn on educational mode
 * Educational mode should...
   - Explain various sources
     * Why does the probe get stuck on the BH-horizon?
     * Precession of the orbit in relativity
   - Explain topologies
     * What is a fundamental polygon?

### version 1.B.0: More physics

 * Add charges
 * Reissner-Nordström/Einstein-Maxwell physics
 * Rotating black holes
 * Relativistic physics in other dimensions?
 * Exotic physics?
 * More topologies:
   - sphere
   - real projective plane
   - infinite Möbius strip

### version 1.C.0: Map editor/sandbox

 * Sandbox play-mode where you can:
   - add sources
   - change physics
   - change topology
   - play the map

### version 2.0.0: Second complete game

This version should have:

 * The user should have more possibility to explore the physics contained in the game
   - Build maps themselves and try them out
   - Play maps where the various physics concepts are explained
 * It should also contain more levels and new physics compared to 1.0.0
 * Have a splash-screen and logo

### version X.Y.0: Other ideas and issues

 * ~~Moving sources~~
 * Figure out a work-around for resetting level (background stars) when menu is spawned.
 * ~~Black holes & horizons~~
 * Asset handles for meshes?
