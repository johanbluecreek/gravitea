# Gravitea

Gravity game in rust.

## Playing

The game does not have an official release yet, but `master` branch should always be playable. If you have [rust installed](https://www.rust-lang.org/tools/install), clone this repository and play the game by

```
$ cargo run
```

## Contributing

See `ROADMAP.md` to get an overview of the goals.
